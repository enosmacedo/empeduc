<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>EMPEDUC - Plataforma Web para Ensino de Empreendedorismo e Finanças</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<style>

    .background {
        fill: none;
        pointer-events: all;
    }

    #states {
        fill: #aaa;
    }

    #states .active {
        fill: orange;
    }

    #state-borders {
        fill: none;
        stroke: #fff;
        stroke-width: 1.5px;
        stroke-linejoin: round;
        stroke-linecap: round;
        pointer-events: none;
    }
	
	div#mapa{	
		margin-left: 400px;
		height: 550px;
	}
	div#restaurante{
		margin-top: 150px;
	}
	#jogo{
		width: 200px;
		height: 50px;
		color: black;
		background-color: white;
		font-size: 18px;
	}
	#bairro{
		width: 100px;
		color: black;
	}
	#empRestaurante{
		height: 100px;
		width: 100px;
		margin-left: 235px;
		margin-top: 100px;
	}
	#empSorveteria{
		height: 110px;
		width: 110px;
		margin-top: 100px;
		margin-left: 20px;
	}

</style>
  
<!-- BEGIN PLUGIN CSS -->
<link href="${resource(dir: 'template', file:'plugins/pace/pace-theme-flash.css')}"  rel="stylesheet" type="text/css" media="screen"/>
<!-- END PLUGIN CSS -->
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap-theme.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/font-awesome/css/font-awesome.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/animate.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.css')}"  rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link href="${resource(dir: 'template', file:'css/styleTeste.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/responsive.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/custom-icon-set.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/talkballon.css')}"  rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
	<div class="header-seperation"> 
		<ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
		 <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
		</ul>
      <!-- BEGIN LOGO -->	
      <!-- END LOGO --> 
      <!--<ul class="nav pull-right notifcation-center">	
        <li class="dropdown" id="header_task_bar"> <a href="index.html" class="dropdown-toggle active" data-toggle=""> <div class="iconset top-home"></div> </a> </li>
        <li class="dropdown" id="header_inbox_bar" > <a href="email.html" class="dropdown-toggle" > <div class="iconset top-messages"></div>  <span class="badge" id="msgs-badge">2</span> </a></li>
		<li class="dropdown" id="portrait-chat-toggler" style="display:none"> <a href="#sidr" class="chat-menu-toggle"> <div class="iconset top-chat-white "></div> </a> </li>        
      </ul> --> 
      </div>
      
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav" > 
      	
		<div class="pull-right"> 
			
			<div class="chat-toggler">	
				<div class="profile-pic"> 
					<img src="${resource(dir: 'template', file:'/img/profiles/teacher-icon.png')}"  alt="" data-src="${resource(dir: 'template', file:'/img/profiles/avatar_small.jpg')}" data-src-retina="assets/img/profiles/avatar_small2x.jpg')}" width="35" height="35" />  
				</div>

				<div id="divSaldo">
					
					<fieldset>
						<legend id="saldo"></legend>
						
					</fieldset>

				</div>

				<div class="talkballon">
					<div class="talk-bubble tri-right right-in">
						<div class="talktext">
							<p id="talkballon"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu"> 
  <!-- BEGIN MINI-PROFILE -->
	   <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
		   <div class="user-info-wrapper">	
				<div class="profile-wrapper">
					<img src="${resource(dir: 'template', file:'/img/aluno.png')}"  alt="" data-src="${resource(dir: 'template', file:'/img/profiles/avatar.jpg')}" data-src-retina="assets/img/profiles/avatar2x.jpg')}" width="69" height="69" />-->
				</div>
				<div class="user-info">
				  <div class="greeting">Bem-vindo,</div>
				  <div class="username">Daniel <span class="semi-bold">Enos</span></div>
				  <div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div>
				</div>
		   </div>
	  <!-- END MINI-PROFILE -->
	   
	   <!-- BEGIN SIDEBAR MENU -->	
		<ul>	
		  <li class=""> <a href="#"> <i class="fa fa-flag"></i>  <span class="title">Perfil</span></a></li>

		  <li class=""> <a href="index.html"> <i class="fa fa-flag"></i>  <span class="title">Conteúdos</span></a></li>
				
		  <li class="active open "> <a href="javascript:;"> <i class="fa fa fa-adjust"></i> <span class="title">Fórum</span> <span class="arrow open"></span> </a>
            <ul class="sub-menu">
              <li > <a id="botao_promocao_criar" >Minhas Mensagens</a> </li>
              <li > <a id="botao_promocao_editar">Mensagens do Professor </a> </li>
			  <li > <a id="botao_promocao_premiar">Premiar </a> </li>
			  
            </ul>
		  </li>  	
		  <li class=""> <a href="#"> <i class="fa fa-flag"></i>  <span class="title">Sair</span></a></li>
		  
		</ul>	
		<!-- END SIDEBAR MENU --> 
	  </div>
  </div>
  <!-- END SIDEBAR --> 

  <div id="empreendimento">
  	<center>
  		<img id="empRestaurante" src="${resource(dir: 'template', file:'/img/restaurante.png')}" />
		<img id="empSorveteria" src="${resource(dir: 'template', file:'/img/sorveteria2.png')}" />
  	</center>
  </div>
  <div id="mapa" ></div>
  
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content"> 
	
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">  
		<div class="page-title">	
			<h3>Tarefas</h3>
			
			<center><h3 id="bairro"></h3></center>
			<center><h2 id="h3empreendimento"></h2></center>
			<!-- BEGIN --> 
			<div class="row">
				<div class="col-md-6">
					<div class="grid simple">
						<div class="grid-title no-border">
							<h4>O que<span class="semi-bold"> posso fazer</span></h4>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
							</div>
						</div>
						<div class="grid-body no-border">
							<div class="row-fluid">
								<!-- Conteudo -->
										
								<b> Estrutura  </b></br>
									<div>
										<font size="3">
											<a id="atv1"  href="#" style="margin-left:10px"></a></br>
										</font>
										<a class="custoatv" style="margin-left:40px">Custo: 50</a></br>
										<a class="qntClientesAtv" style="margin-left:40px">Quantidade de clientes: 50</a></br>
									</div>
									</br>
									<a id="atv2" href="#" style="margin-left:10px"></a></br>
									<a id="atv7" href="#" style="margin-left:10px"></a></br>
									<a id="atv8" href="#" style="margin-left:10px"></a></br>
									<a id="atv9" href="#" style="margin-left:10px"></a></br>
									
									<hr/>
								<b> Burocracia </b></br>
									<a id="atv3" href="#" style="margin-left:10px"></a></br>
									<a id="atv4" href="#" style="margin-left:10px"></a></br>
									<a id="atv13" href="#" style="margin-left:10px"></a></br>
									<a id="atv14" href="#" style="margin-left:10px"></a></br>									
									<hr/>
								<b> Finanças  </b></br>
									<a id="atv5" href="#" style="margin-left:10px"></a></br>
									<a id="atv6" href="#" style="margin-left:10px"></a></br>
									<a id="atv15" href="#" style="margin-left:10px"></a></br>
									<a id="atv16" href="#" style="margin-left:10px"></a></br>
									<a id="atv25" href="#" style="margin-left:10px"></a></br>
									<a id="atv26" href="#" style="margin-left:10px"></a></br>
									
									<hr/>
								<b> Serviços  </b></br>
									<a id="atv17" href="#" style="margin-left:10px"></a></br>
									<a id="atv18" href="#" style="margin-left:10px"></a></br>
									<a id="atv19" href="#" style="margin-left:10px"></a></br>
									<a id="atv20" href="#" style="margin-left:10px"></a></br>
									<a id="atv21" href="#" style="margin-left:10px"></a></br>
									<a id="atv22" href="#" style="margin-left:10px"></a></br>
									<a id="atv23" href="#" style="margin-left:10px"></a></br>
									<a id="atv24" href="#" style="margin-left:10px"></a></br>
									
									<hr/>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="grid simple">
						<div class="grid-title no-border">
							<h4>O que <span class="semi-bold">vou fazer</span></h4>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
							</div>
						</div>
						<div class="grid-body no-border">
							<div class="row-fluid">
								<!-- BEGIN BODY --> 
								<b> Estrutura  </b></br>
									<a id="atvd1" href="#" style="margin-left:10px"></a></br>
									<a id="atvd2" href="#" style="margin-left:10px"></a></br>
									<a id="atvd7" href="#" style="margin-left:10px"></a></br>
									<a id="atvd8" href="#" style="margin-left:10px"></a></br>
									<a id="atvd9" href="#" style="margin-left:10px"></a></br>
									
									<hr/>
								<b> Burocracia </b></br>
									<a id="atvd3" href="#" style="margin-left:10px"></a></br>
									<a id="atvd4" href="#" style="margin-left:10px"></a></br>
									<a id="atvd13" href="#" style="margin-left:10px"></a></br>
									<a id="atvd14" href="#" style="margin-left:10px"></a></br>									
									<hr/>
								<b> Finanças  </b></br>
									<a id="atvd5" href="#" style="margin-left:10px"></a></br>
									<a id="atvd6" href="#" style="margin-left:10px"></a></br>
									<a id="atvd15" href="#" style="margin-left:10px"></a></br>
									<a id="atvd16" href="#" style="margin-left:10px"></a></br>
									<a id="atvd25" href="#" style="margin-left:10px"></a></br>
									<a id="atvd26" href="#" style="margin-left:10px"></a></br>
									
									<hr/>
								<b> Serviços  </b></br>
									<a id="atvd17" href="#" style="margin-left:10px"></a></br>
									<a id="atvd18" href="#" style="margin-left:10px"></a></br>
									<a id="atvd19" href="#" style="margin-left:10px"></a></br>
									<a id="atvd20" href="#" style="margin-left:10px"></a></br>
									<a id="atvd21" href="#" style="margin-left:10px"></a></br>
									<a id="atvd22" href="#" style="margin-left:10px"></a></br>
									<a id="atvd23" href="#" style="margin-left:10px"></a></br>
									<a id="atvd24" href="#" style="margin-left:10px"></a></br>
									
									<hr/>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			
		</div>
		<center><button id="jogo">Jogar</button></center>
		
    </div>
  </div>  
 <!-- END CONTAINER --> 
</div>
<!-- END CONTAINER -->

<!-- BEGIN CORE JS FRAMEWORK--> 
<script src="plugins/jquery-1.8.3.min.js" type="text/javascript"></script> 
<script src="plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="plugins/breakpoints.js" type="text/javascript"></script> 
<script src="plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script> 
<script src="plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<!-- END CORE JS FRAMEWORK --> 
<!-- BEGIN PAGE LEVEL JS --> 
<script src="plugins/pace/pace.min.js" type="text/javascript"></script>  
<script src="plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS --> 	

<!-- BEGIN CORE TEMPLATE JS --> 
<script src="js/core.js" type="text/javascript"></script> 
<script src="js/chat.js" type="text/javascript"></script> 
<script src="js/Biblioteca.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS --> 
<!-- BEGIN D3JS FRAMEWORK -->
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="d3.v3.min.js"></script>
<script src="topojson.v1.min.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairro.js"></script>
<script src="JogoEMPEDUC/Estrutura/Sorveteria.js"></script>
<script src="JogoEMPEDUC/Estrutura/Acoes.js"></script>
<script src="JogoEMPEDUC/Estrutura/Comprar_Cadeira.js"></script>
<script src="JogoEMPEDUC/Estrutura/Empreendimento.js"></script>
<script src="JogoEMPEDUC/Estrutura/Fazer_Alvara.js"></script>
<script src="JogoEMPEDUC/Estrutura/Fazer_CNPJ.js"></script>


<script src="JogoEMPEDUC/Estrutura/Bairros/Texas.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Alabama.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Alasca.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Arcansas.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Arizona.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/California.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Cansas.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Carolina_do_Norte.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Carolina_do_Sul.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Colorado.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Conecticute.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Dacota_do_Norte.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Dacota_do_Sul.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Delaware.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Florida.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Georgia.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Havai.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Idaho.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Ilha_de_Rodes.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Ilinois.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Indiana.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Iowa.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Kentucky.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Luisiana.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Maine.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Marilandia.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Massachussets.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Michigan.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Minesota.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Mississipi.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Missuri.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Montana.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Nebrasca.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Nevada.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Nova_Hampshire.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Nova_Iorque.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Nova_Jersei.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Novo_Mexico.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Ohio.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Oklahoma.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Oregon.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Pensilvania.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Tenessi.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Uta.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Vermonte.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Virginia.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Virginia_Ocidental.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Washington.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Washington_DC.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Wisconsin.js"></script>
<script src="JogoEMPEDUC/Estrutura/Bairros/Wyoming.js"></script>

<script src="JogoEMPEDUC/Estrutura/Restaurante.js"></script>
<script src="JogoEMPEDUC/Estrutura/Comprar_Balcao.js"></script>
<script src="JogoEMPEDUC/Estrutura/Comprar_Mesa.js"></script>
<script src="JogoEMPEDUC/Estrutura/Comprar_Materia_Prima_para_Comida_Padrao_Baixo.js"></script>
<script src="JogoEMPEDUC/Estrutura/Comprar_Materia_Prima_para_Comida_Padrao_Alto.js"></script>
<script src="JogoEMPEDUC/Estrutura/Comprar_Materia_Prima_para_Comida_Padrao_Medio.js"></script>
<script src="JogoEMPEDUC/Estrutura/Fazer_Publicidade_Complexa.js"></script>
<script src="JogoEMPEDUC/Estrutura/Fazer_Publicidade_TV.js"></script>
<script src="JogoEMPEDUC/Estrutura/Fazer_Publicidade_Internet.js"></script>
<script src="JogoEMPEDUC/Estrutura/Fazer_Reforma.js"></script>
<script src="JogoEMPEDUC/Estrutura/Pagar_Contas.js"></script>
<script src="JogoEMPEDUC/Estrutura/Comprar_Eletrodomesticos.js"></script>
<script src="JogoEMPEDUC/Estrutura/Fazer_Publicidade_Panfleto.js"></script>
<script src="JogoEMPEDUC/Estrutura/Pagar_Impostos.js"></script>
<script src="JogoEMPEDUC/Estrutura/Comprar_Aparelhos_Eletronicos.js"></script>
<script src="JogoEMPEDUC/Estrutura/Contratar_Funcionario.js"></script>
<script src="JogoEMPEDUC/Estrutura/Fazer_Publicidade_Radio.js"></script>
<script src="JogoEMPEDUC/Estrutura/Qualificar_Mao_de_Obra.js"></script>
<script src="JogoEMPEDUC/Estrutura/Demitir_Funcionario.js"></script>
<script src="JogoEMPEDUC/Estrutura/Contratar_Seguranca_Desarmada.js"></script>
<script src="JogoEMPEDUC/Estrutura/Contratar_Seguranca_Armada.js"></script>

<script>

	var width = 850,
			height = 610,
			centered;

	var projection = d3.geo.albersUsa()
			.scale(1070)
			.translate([width / 2, height / 2]);

	var path = d3.geo.path()
			.projection(projection);

	var svg = d3.select("div#mapa").append("svg")
			.attr("width", width)
			.attr("height", height);

	svg.append("rect")
			.attr("class", "background")
			.attr("width", width)
			.attr("height", height)
			.on("click", clicked);

	var g = svg.append("g");

	d3.json("us.json", function (error, us) {
		if (error)
			throw error;

		g.append("g")
				.attr("id", "states")
				.selectAll("path")
				.data(topojson.feature(us, us.objects.states).features)
				.enter().append("path")
				.attr("d", path)
				.on("click", clicked);

		g.append("path")
				.datum(topojson.mesh(us, us.objects.states, function (a, b) {
					return a !== b;
				}))
				.attr("id", "state-borders")
				.attr("d", path);
	});

	function exibirInforBairro(bairroTeste){
			
		var mensagem = "Bairro: " + bairroTeste.nome +" \n\nÍndice Fluvial: " +
		bairroTeste.indice_fluvial + "\n\nÍndice de Desastre Ambiental: " +
		bairroTeste.indice_desastre_ambiental + "\n\nRenda per Capta: " +
		bairroTeste.renda_per_capta + "\n\nÍndice de Violência: " +
		bairroTeste.indice_violencia + "\n\nQuantidade de Casas: " +
		bairroTeste.qnt_casas + "\n\nQuantidade de Apartamentos: " +
		bairroTeste.qnt_apartamentos + "\n\nTaxa de Migração: " +
		bairroTeste.taxa_de_migracao;

		return mensagem;
			
	}

	function clicked(d) {

		switch(d.id){
			case 48:
				var bairroTeste = new Texas();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Texas();
					$("#mapa").hide();
				}
				break;
			case 53:
				var bairroTeste = new Washington();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Washington();
					$("#mapa").hide();
				}
				break;
			case 41:
				var bairroTeste = new Oregon();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Oregon();
					$("#mapa").hide();
				}
				break;
			case 6:
				var bairroTeste = new California();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new California();
					$("#mapa").hide();
				}
				break;
			case 16:
				var bairroTeste = new Idaho();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Idaho();
					$("#mapa").hide();
				}
				break;
			case 32:
				var bairroTeste = new Nevada();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Nevada();
					$("#mapa").hide();
				}
				break;
			case 49:
				var bairroTeste = new Uta();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Uta();
					$("#mapa").hide();
				}
				break;
			case 4:
				var bairroTeste = new Arizona();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Arizona();
					$("#mapa").hide();
				}
				break;
			case 30:
				var bairroTeste = new Montana();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Montana();
					$("#mapa").hide();
				}
				break;
			case 56:
				var bairroTeste = new Wyoming();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Wyoming();
					$("#mapa").hide();
				}
				break;
			case 8:
				var bairroTeste = new Colorado();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Colorado();
					$("#mapa").hide();
				}
				break;
			case 35:
				var bairroTeste = new Novo_Mexico();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Novo_Mexico();
					$("#mapa").hide();
				}
				break;
			case 38:
				var bairroTeste = new Dacota_do_Norte();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Dacota_do_Norte();
					$("#mapa").hide();
				}
				break;
			case 46:
				var bairroTeste = new Dacota_do_Sul();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Dacota_do_Sul();
					$("#mapa").hide();
				}
				break;
			case 31:
				var bairroTeste = new Nebrasca();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Nebrasca();
					$("#mapa").hide();
				}
				break;
			case 20:
				var bairroTeste = new Cansas();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Cansas();
					$("#mapa").hide();
				}
				break;
			case 40:
				var bairroTeste = new Oklahoma();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Oklahoma();
					$("#mapa").hide();
				}
				break;
			case 27:
				var bairroTeste = new Minesota();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Minesota();
					$("#mapa").hide();
				}
				break;
			case 19:
				var bairroTeste = new Iowa();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Iowa();
					$("#mapa").hide();
				}
				break;
			case 29:
				var bairroTeste = new Missuri();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Missuri();
					$("#mapa").hide();
				}
				break;
			case 5:
				var bairroTeste = new Arcansas();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Arcansas();
					$("#mapa").hide();
				}
			case 22:
				var bairroTeste = new Luisiana();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Luisiana();
					$("#mapa").hide();
				}
				break;
			case 55:
				var bairroTeste = new Wisconsin();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Wisconsin();
					$("#mapa").hide();
				}
				break;			
			case 17:
				var bairroTeste = new Ilinois();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Ilinois();
					$("#mapa").hide();
				}
				break;
			case 21:
				var bairroTeste = new Kentucky();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Kentucky();
					$("#mapa").hide();
				}
				break;
			case 47:
				var bairroTeste = new Tenessi();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Tenessi();
					$("#mapa").hide();
				}
				break;
			case 28:
				var bairroTeste = new Mississipi();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Mississipi();
					$("#mapa").hide();
				}
				break;
			case 26:
				var bairroTeste = new Michigan();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Michigan();
					$("#mapa").hide();
				}
				break;
			case 18:
				var bairroTeste = new Indiana();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Indiana();
					$("#mapa").hide();
				}
				break;
			case 39:
				var bairroTeste = new Ohio();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Ohio();
					$("#mapa").hide();
				}
				break;
			case 54:
				var bairroTeste = new Virginia_Ocidental();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Virginia_Ocidental();
					$("#mapa").hide();
				}
				break;
			case 1:
				var bairroTeste = new Alabama();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Alabama();
					$("#mapa").hide();
				}
				break;
			case 51:
				var bairroTeste = new Virginia();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Virginia();
					$("#mapa").hide();
				}
				break;
			case 37:
				var bairroTeste = new Carolina_do_Norte();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Carolina_do_Norte();
					$("#mapa").hide();
				}
				break;
			case 45:
				var bairroTeste = new Carolina_do_Sul();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Carolina_do_Sul();
					$("#mapa").hide();
				}
				break;
			case 13:
				var bairroTeste = new Georgia();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Georgia();
					$("#mapa").hide();
				}
				break;
			case 12:
				var bairroTeste = new Florida();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Florida();
					$("#mapa").hide();
				}
				break;
			case 42:
				var bairroTeste = new Pensilvania();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Pensilvania();
					$("#mapa").hide();
				}
				break;
			case 34:
				var bairroTeste = new Nova_Jersei();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Nova_Jersei();
					$("#mapa").hide();
				}
				break;
			case 36:
				var bairroTeste = new Nova_Iorque();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Nova_Iorque();
					$("#mapa").hide();
				}
				break;
			case 24:
				var bairroTeste = new Marilandia();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Marilandia();
					$("#mapa").hide();
				}
				break;
			case 10:
				var bairroTeste = new Delaware();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Delaware();
					$("#mapa").hide();
				}
				break;
			case 9:
				var bairroTeste = new Conecticute();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Conecticute();
					$("#mapa").hide();
				}
				break;
			case 33:
				var bairroTeste = new Nova_Hampshire();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Nova_Hampshire();
					$("#mapa").hide();
				}
				break;
			case 23:
				var bairroTeste = new Maine();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Maine();
					$("#mapa").hide();
				}
				break;
			case 50:
				var bairroTeste = new Vermonte();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Vermonte();
					$("#mapa").hide();
				}
				break;
			case 25:
				var bairroTeste = new Massachussets();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Massachussets();
					$("#mapa").hide();
				}
				break;				
			case 44:
				var bairroTeste = new Ilha_de_Rodes();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Ilha_de_Rodes();
					$("#mapa").hide();
				}
				break;
			case 2:
				var bairroTeste = new Alasca();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Alasca();
					$("#mapa").hide();
				}
				break;
			case 15:
				var bairroTeste = new Havai();
			
				if(confirm(exibirInforBairro(bairroTeste))){
					bairro = new Havai();
					$("#mapa").hide();
				}
				break;
		}
		setFala("Você já pode começar a jogar!\nInicialmente você irá fazer seu CPNJ e providenciar seu Alvará para poder começar a empreender.","#talkballon");
		setTempo(150,".profile-pic",".talkballon");
		
		$("#atv3").hide();
		$("#atv4").hide();
		$("#atvd3").show();
		$("#atvd4").show();
		
		
		var x, y, k;

		if (d && centered !== d) {
			var centroid = path.centroid(d);
			x = centroid[0];
			y = centroid[1];
			k = 4;
			centered = d;
		} else {
			x = width / 2;
			y = height / 2;
			k = 1;
			centered = null;
		}

		g.selectAll("path")
				.classed("active", centered && function (d) {
					return d === centered;
				});

		g.transition()
				.duration(750)
				//.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
				.style("stroke-width", 1.5 / k + "px");
		
		//alert(d.id);
		//alert('Seleção: ' + JSON.stringify(d));
	}

	$("#mapa").hide();
		
	function setFala(texto,elemento){
		$(elemento).text(texto);
	}

	function setTempo(tempo,elemento,elemento2){
		$(elemento).click(function(){
			$(elemento2).show()
				.slideUp(tempo);
		});
	}
	
	$(".custoatv").click(function(){
		alert("Custo é o quanto de capital(dinheiro) necessário para poder realizar a ação. Será retirado diretamente do capital do seu estabelecimento.");
	});
	
	$(".qntClientesAtv").click(function(){
		alert("Esse é o limitante de clientes que seu estabelecimento pode atender. No caso, quanto mais clientes puder atender, mais chances terá de gerar lucro.");
	});

	$("#empSorveteria").click(function(){
		if(confirm("Tem certeza que deseja empreender com uma sorveteria?")){
			empreendimento = new Sorveteria();
			empreendimento.renda = 10000;
			empreendimento.nome = "Sorveteria";
			empreendimento.limitanteClientes = 0;
			empreendimento.cnpj = false;
			empreendimento.alvara = false;
			$("#empRestaurante").hide();
			$("#empSorveteria").hide();
			$("#mapa").show();
			setFala("Agora é hora de escolher o bairro onde você vai empreender. Lembre-se de escolher com calma de acordo com as características de cada bairro. Os índices de cada bairro vão influenciar na gestão do seu empreendimento.","#talkballon");
			setTempo(150,".profile-pic",".talkballon");
		}
		
	});
	
	$("#empRestaurante").click(function(){
		if(confirm("Tem certeza que deseja empreender com um restaurante?")){
			empreendimento = new Restaurante();
			empreendimento.nome = "Restaurante";
			$("#empRestaurante").hide();
			$("#empSorveteria").hide();
			$("#mapa").show();
			setFala("Agora é hora de escolher o bairro onde você vai empreender. Lembre-se de escolher com calma de acordo com as características de cada bairro. Os índices de cada bairro vão influenciar na gestão do seu empreendimento.","#talkballon");
			setTempo(150,".profile-pic",".talkballon");
		}
		
	});
	
	setFala("Olá, jovem gafanhoto. Seja muito bem-vindo ao game EMPEDUC!\nPara começar a empreender, é necessário a escolha do seu empreendimento e o bairro onde você vai gerir seu negócio!\nAs opções atuais são restaurante e sorveteria.","#talkballon");
	setTempo(3000,".profile-pic",".talkballon");
	
	var atvs = [];
	atvs[0] = "Comprar Balcão";
	atvs[1] = "Comprar Cadeira";
	atvs[2] = "Comprar Eletrodomésticos";
	atvs[3] = "Comprar matéria-prima para comida de alto padrão";
	atvs[4] = "Comprar matéria-prima para comida de médio padrão";
	atvs[5] = "Comprar matéria-prima para comida de baixo padrão";
	atvs[6] = "Comprar Mesa";
	atvs[7] = "Contratar funcionário";
	atvs[8] = "Demitir funcionário";
	atvs[9] = "Fazer Alvará";
	atvs[10] = "Fazer CNPJ";
	atvs[11] = "Fazer Publicidade Complexa";
	atvs[12] = "Fazer Publicidade na Internet";
	atvs[13] = "Fazer Publicidade em Panfletos";
	atvs[14] = "Fazer Publicidade no Rádio";
	atvs[15] = "Fazer Publicidade na TV";
	atvs[16] = "Fazer reforma no estabelecimento";
	atvs[17] = "Qualificar mão de obra";
	atvs[18] = "Pagar Impostos";
	atvs[19] = "Comprar Aparelhos Eletrônicos";
	atvs[20] = "Pagar Contas";
	atvs[21] = "Contratar Segurança Desarmada";
	atvs[22] = "Contratar Segurança Armada";
	
	//Estrutura
	$("#atv1").text(atvs[0]);
	$("#atv2").text(atvs[1]);
	$("#atv7").text(atvs[6]);
	$("#atv8").text(atvs[2]);
	$("#atv9").text(atvs[19]);
	
	
	//Burocracia
	$("#atv3").text(atvs[9]);
	$("#atv4").text(atvs[10]);
	$("#atv13").text(atvs[18]);
	$("#atv14").text(atvs[20]);
	
	//Finanças
	$("#atv5").text(atvs[7]);
	$("#atv6").text(atvs[8]);
	$("#atv15").text(atvs[17]);
	$("#atv16").text(atvs[16]);
	$("#atv25").text(atvs[21]);
	$("#atv26").text(atvs[22]);
	
	//Serviços
	$("#atv17").text(atvs[11]);
	$("#atv18").text(atvs[12]);
	$("#atv19").text(atvs[13]);
	$("#atv20").text(atvs[14]);
	$("#atv21").text(atvs[15]);
	$("#atv22").text(atvs[3]);
	$("#atv23").text(atvs[4]);
	$("#atv24").text(atvs[5]);
	
	function escolherTarefa(elemento,elemento2,texto){
		$(elemento).click(function(){
			if(confirm("Tem certeza que deseja escolher esta tarefa?")){
				$(elemento2).show();
				$(elemento2).text(texto);
				$(elemento).css("display", "none");
			}
		});
	}
	
	function voltarTarefa(elemento,elemento2){
		$(elemento).click(function(){
			if(confirm("Tem certeza que deseja cancelar esta tarefa?")){
				$(elemento).hide();
				$(elemento2).show();
			}
		});
	}
	
	//Estrutura
	escolherTarefa("#atv1","#atvd1",atvs[0]);
	escolherTarefa("#atv2","#atvd2",atvs[1]);
	escolherTarefa("#atv7","#atvd7",atvs[6]);
	escolherTarefa("#atv8","#atvd8",atvs[2]);
	escolherTarefa("#atv9","#atvd9",atvs[19]);
	
	voltarTarefa("#atvd1","#atv1");
	voltarTarefa("#atvd2","#atv2");
	voltarTarefa("#atvd7","#atv7");
	voltarTarefa("#atvd8","#atv8");
	voltarTarefa("#atvd9","#atv9");
	
	
	//Burocracia
	escolherTarefa("#atv3","#atvd3",atvs[9]);
	escolherTarefa("#atv4","#atvd4",atvs[10]);
	escolherTarefa("#atv13","#atvd13",atvs[18]);
	escolherTarefa("#atv14","#atvd14",atvs[20]);
	
	voltarTarefa("#atvd3","#atv3");
	voltarTarefa("#atvd4","#atv4");
	voltarTarefa("#atvd13","#atv13");
	voltarTarefa("#atvd14","#atv14");
	
	//Finanças
	escolherTarefa("#atv5","#atvd5",atvs[7]);
	escolherTarefa("#atv6","#atvd6",atvs[8]);
	escolherTarefa("#atv15","#atvd15",atvs[17]);
	escolherTarefa("#atv16","#atvd16",atvs[16]);
	escolherTarefa("#atv25","#atvd25",atvs[21]);
	escolherTarefa("#atv26","#atvd26",atvs[22]);
	
	voltarTarefa("#atvd5","#atv5");
	voltarTarefa("#atvd6","#atv6");
	voltarTarefa("#atvd15","#atv15");
	voltarTarefa("#atvd16","#atv16");
	voltarTarefa("#atvd25","#atv25");
	voltarTarefa("#atvd26","#atv26");
	
	//Serviços
	escolherTarefa("#atv17","#atvd17",atvs[11]);
	escolherTarefa("#atv18","#atvd18",atvs[12]);
	escolherTarefa("#atv19","#atvd19",atvs[13]);
	escolherTarefa("#atv20","#atvd20",atvs[14]);
	escolherTarefa("#atv21","#atvd21",atvs[15]);
	escolherTarefa("#atv22","#atvd22",atvs[3]);
	escolherTarefa("#atv23","#atvd23",atvs[4]);
	escolherTarefa("#atv24","#atvd24",atvs[5]);
	
	voltarTarefa("#atvd17","#atv17");
	voltarTarefa("#atvd18","#atv18");
	voltarTarefa("#atvd19","#atv19");
	voltarTarefa("#atvd20","#atv20");
	voltarTarefa("#atvd21","#atv22");
	voltarTarefa("#atvd23","#atv23");
	voltarTarefa("#atvd24","#atv24");	
	
	var visiveis = [];

	$("#saldo").hide();
	
	$("#atv3").hide();
	$("#atv4").hide();
	$("#atvd3").show();
	$("#atvd3").text(atvs[9]);
	$("#atvd4").show();
	$("#atvd4").text(atvs[10]);
	
	$("#jogo").click(function(){
		var alvara = $('#atvd3').is(':visible');
		var cnpj = $('#atvd4').is(':visible');
		var restaurante = $('#empRestaurante').is(':visible');
		var sorveteria = $('#empSorveteria').is(':visible');
		var mapa = $('#mapa').is(':visible');
		
		if(alvara == true && cnpj == true){
			var acaoCNPJ = new Fazer_Alvara();
			var acaoCNPJ = new Fazer_CNPJ();
			empreendimento.renda -= acaoCNPJ.valor;
			empreendimento.renda -= acaoCNPJ.valor;
			empreendimento.cnpj = true;
			empreendimento.alvara = true;
			$("#atvd3").hide();
			$("#atvd4").hide();
		}

		if(restaurante == false && sorveteria == false && mapa == false){
			if(empreendimento.cnpj == true && empreendimento.alvara == true){
		
				setFala("Seu desafio começou!\nPara cada tarefa que você for fazer há um custo e posteriormente pode refletir em lucro ou prejuízo. Articule calmamente suas ações e boa sorte!","#talkballon");
				setTempo(150,".profile-pic",".talkballon");

				$("#saldo").show();
				$("#saldo").text(empreendimento.renda);

				$("#bairro").text(bairro.nome);
				$("#h3empreendimento").text(empreendimento.nome);

				visiveis[0] = $('#atv1').is(':visible');
				visiveis[1] = $('#atv2').is(':visible');
				visiveis[2] = $('#atv7').is(':visible');
				visiveis[3] = $('#atv8').is(':visible');
				visiveis[4] = $('#atv9').is(':visible');
				visiveis[5] = $('#atv3').is(':visible');
				visiveis[6] = $('#atv4').is(':visible');
				visiveis[7] = $('#atv13').is(':visible');
				visiveis[8] = $('#atv14').is(':visible');
				visiveis[9] = $('#atv5').is(':visible');
				visiveis[10] = $('#atv6').is(':visible');
				visiveis[11] = $('#atv15').is(':visible');
				visiveis[12] = $('#atv16').is(':visible');
				visiveis[13] = $('#atv17').is(':visible');
				visiveis[14] = $('#atv18').is(':visible');
				visiveis[15] = $('#atv19').is(':visible');
				visiveis[16] = $('#atv20').is(':visible');
				visiveis[17] = $('#atv21').is(':visible');
				visiveis[18] = $('#atv22').is(':visible');
				visiveis[19] = $('#atv23').is(':visible');
				visiveis[20] = $('#atv24').is(':visible');
				visiveis[21] = $('#atv25').is(':visible');
				visiveis[22] = $('#atv26').is(':visible');
				
					
				var acoes = [];
				acoes[0] = new Comprar_Balcao();
				acoes[1] = new Comprar_Cadeira();
				acoes[2] = new Comprar_Mesa();
				acoes[3] = new Comprar_Eletrodomesticos();
				acoes[4] = new Comprar_Aparelhos_Eletronicos();
				acoes[5] = new Fazer_Alvara();
				acoes[6] = new Fazer_CNPJ();
				acoes[7] = new Pagar_Impostos();
				acoes[8] = new Pagar_Contas();
				acoes[9] = new Contratar_Funcionario();
				acoes[10] = new Demitir_Funcionario();
				acoes[11] = new Qualificar_Mao_de_Obra();
				acoes[12] = new Fazer_Reforma();
				acoes[13] = new Fazer_Publicidade_Complexa();
				acoes[14] = new Fazer_Publicidade_Internet();
				acoes[15] = new Fazer_Publicidade_Panfleto();
				acoes[16] = new Fazer_Publicidade_Radio();
				acoes[17] = new Fazer_Publicidade_TV();
				acoes[18] = new Comprar_Materia_Prima_para_Comida_Padrao_Alto();
				acoes[19] = new Comprar_Materia_Prima_para_Comida_Padrao_Medio();
				acoes[20] = new Comprar_Materia_Prima_para_Comida_Padrao_Baixo();
				acoes[21] = new Contratar_Seguranca_Desarmada();
				acoes[22] = new Contratar_Seguranca_Armada();
				
				//O jogador já vai ter alvará e CNPJ, então o valor vai ser 0.
				acoes[5].valor = 0;
				acoes[6].valor = 0;
				
				contador = 0;
				for(var i=0; i<23; i++){

					if(visiveis[i] == false){
						contador += 1;
						empreendimento.renda = empreendimento.renda - acoes[i].valor;
						
						empreendimento.limitanteClientes += acoes[i].getQnt_clientes_consigo_atender(30);
						
						if(empreendimento.renda <= 0){
							alert("Você faliu! O jogo será reiniciado para você tentar de novo.");
							window.location.reload();
						}
						
						var prob = (1 * Math.random());
						
						for(var j=0; j < empreendimento.limitanteClientes; j++){
							prob = (1 * Math.random());
							if(prob >= 0.5){
								empreendimento.renda += acoes[i].getNivel_de_servico_ou_produto_orferido(30);
							}
						}
				
					}
					
				}
				
				if(visiveis[21] == false){
					bairro.indice_violencia = bairro.indice_violencia - 25;
				}else if(visiveis[22] == false){
					bairro.indice_violencia = bairro.indice_violencia - 40;
				}
				
				prob = (100 * Math.random());
				
				if(contador >= 1){
					if(prob <= bairro.indice_violencia){
						alert("Você foi assaltado!");
						empreendimento.renda -= 1500;
					}
				}
				
				$("#saldo").text(empreendimento.renda);
				
				if(empreendimento.renda >= 20000){
					alert("Sucesso! Você empreendeu muito bem!");
					window.location.reload();
				}
			}else{
				alert("Alvará ou CNPJ não providenciados!");
			}
		}else{
			alert("Bairro ou Empreendimento não escolhidos... Tente novamente!");
		}
		
	});
	
	
</script>
</body>
</html>