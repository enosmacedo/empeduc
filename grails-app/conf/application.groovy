

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'comum.Usuario'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'comum.UsuarioPermissao'
grails.plugin.springsecurity.authority.className = 'comum.Permissao'


grails.plugin.springsecurity.auth.loginFormUrl = '/areaRestrita/login'
grails.plugin.springsecurity.afterLogoutUrl = '/areaRestrita/logout'
grails.plugin.springsecurity.sucessHandler.defaultTargetUrl = '/areaRestrita/admin'
grails.plugin.springsecurity.failureHandler.defaultFailureUrl = '/areaRestrita/erro'
grails.plugin.springsecurity.sucessHandler.alwaysUseDefault = false
grails.plugin.springsecurity.dao.hideUserNotFoundExceptions = false
grails.plugin.springsecurity.adh.errorPage = '/j_spring_security_logout'
grails.plugin.springsecurity.password.algorithm = 'SHA-256'

grails.plugin.springsecurity.securityConfigType = 'InterceptUrlMap'
grails.plugin.springsecurity.interceptUrlMap = [
    [pattern: '/pessoa',               access: ['ROLE_ADMIN']],
    [pattern: '/diretor/create',       access: ['ROLE_ADMIN']],
    [pattern: '/diretor/show/**',      access: ['ROLE_ADMIN','ROLE_DIRETOR']],
    [pattern: '/diretor/edit/**',      access: ['ROLE_ADMIN','ROLE_DIRETOR']],
    [pattern: '/professor/create',     access: ['ROLE_ADMIN']],
    [pattern: '/professor/show/**',    access: ['ROLE_ADMIN','ROLE_PROFESSOR']],
    [pattern: '/professor/edit/**',    access: ['ROLE_ADMIN','ROLE_PROFESSOR']],
    [pattern: '/aluno/create',         access: ['ROLE_ADMIN']],
    [pattern: '/aluno/show/**',        access: ['ROLE_ADMIN','ROLE_ALUNO']],
    [pattern: '/aluno/edit/**',        access: ['ROLE_ADMIN','ROLE_ALUNO']],
    [pattern: '/diario/create',        access: ['ROLE_DIRETOR','ROLE_ADMIN']],
    [pattern: '/diario/show/**',       access: ['ROLE_DIRETOR','ROLE_ADMIN']],
    [pattern: '/nota/create',          access: ['ROLE_PROFESSOR','ROLE_ADMIN']],    
    [pattern: '/nota/show/**',         access: ['ROLE_PROFESSOR','ROLE_ADMIN']],
    [pattern: '/forum/create',         access: ['ROLE_PROFESSOR','ROLE_ADMIN','ROLE_DIRETOR']],
    [pattern: '/**',                   access: ['IS_AUTHENTICATED_ANONYMOUSLY']]
]
    
    
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

