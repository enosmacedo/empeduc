<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<!DOCTYPE html>
        
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" contet="width=device-width, inicial-scale=1.0">
        <title>Sample title</title>
        
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>EMPEDUC</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN CORE CSS FRAMEWORK -->
        <link href="${resource(dir: 'template', file:'/plugins/pace/pace-theme-flash.css')}" rel="stylesheet" type="text/css" media="screen"/>
        <link href="${resource(dir: 'template', file:'/plugins/boostrapv3/css/bootstrap.min.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/plugins/boostrapv3/css/bootstrap-theme.min.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/plugins/font-awesome/css/font-awesome.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/animate.min.css" rel="stylesheet')}" type="text/css"/>
        <!-- END CORE CSS FRAMEWORK -->
        <!-- BEGIN CSS TEMPLATE -->
        <link href="${resource(dir: 'template', file:'/css/style.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/responsive.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/custom-icon-set.css')}" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="error-body no-top">   
        <div class="container">
          <div class="row login-container column-seperation">  
                <div class="col-md-5 col-md-offset-1">
                  <h1>Login</h1>  
                  <h2>Entre no EMPEDUC</h2>
                  <p>O EMPEDUC é gratuito e sempre será gratuito.</p>
                  <br>
                </div>

                <form action="${resource(file: '/login/authenticate')}" method='POST'  id="frmLogar" name="frmLogar">
                    <div class="col-md-5 "> <br>
                        <div class="row">
                             <div class="form-group col-md-10">
                                <label class="form-label">Login</label>
                                <div class="controls">
                                    <div class="input-with-icon  right">                                       
                                        <i class=""></i>
                                        <input type="text"  id="username" name="username" class="form-control">                                 
                                    </div>
                                </div>
                            </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-10">
                            <label class="form-label">Senha</label>
                            <span class="help"></span>
                                <div class="controls">
                                    <div class="input-with-icon  right">                                       
                                        <i class=""></i>
                                        <input type="password" name="password" id="password" class="form-control">                                 
                                    </div>
                                </div>
                            </div>
                      </div>
                      <div class="row">
                        <div class="col-md-10">
                          <button class="btn btn-primary btn-cons pull-right" type="submit">Login</button>
                        </div>
                      </div>
                    </div>
                </form>
          </div>
        </div>

        <!-- END CONTAINER -->
        <!-- BEGIN CORE JS FRAMEWORK-->
        <script src="${resource(dir: 'template', file:'/plugins/jquery-1.8.3.min.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'template', file:'/plugins/bootstrap/js/bootstrap.min.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'template', file:'/plugins/pace/pace.min.js" type="text/javascript')}"></script>
        <script src="${resource(dir: 'template', file:'/plugins/jquery-validation/js/jquery.validate.min.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'template', file:'/js/login.js" type="text/javascript')}"></script>
        <!-- BEGIN CORE TEMPLATE JS -->
        <!-- END CORE TEMPLATE JS -->
    </body>
</html>
            
