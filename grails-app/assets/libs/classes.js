
    var APPLICATION_ID = '7FAEBD50-05B5-1C7C-FFAB-97545B4FF900';
    var SECRET_KEY = '17777D5F-BF16-80AA-FFFF-C9910FF9BE00';
    var VERSION = 'v1';
    Backendless.serverURL = "https://api.backendless.com";
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);

    function cleanPrivateRelations(data) {
        function isObject(obj) {
            return obj !== null && typeof obj === 'object';
        }

        if (data.hasOwnProperty('_private_relations') && data['_private_relations'].length > 0) {
                data['_private_relations'].forEach(function(relation) {
                    if (data.hasOwnProperty(relation) && isObject(data[relation])) {
                        if (Array.isArray(data[relation])) {
                            data[relation].forEach(function(elem) {
                                if (isObject(elem)) {
                                    cleanPrivateRelations(elem);
                                }
                            });
                        } else {
                            cleanPrivateRelations(data[relation]);
                    }
                }
            });
        }

        if (isObject(data)) {
            delete data['_private_relations'];
            delete data['_private_geoRelations'];
            delete data['_private_dates'];
        }
    }

    
    Message: function Message( args ){
        args = args || {};
        this._private_relations = [
        "company"];
        this._private_geoRelations = [];
        this._private_dates = [
            "created",
            "updated"];
        this.text = args.name || "";
        this.title = args.email || "";
        this.___class = "Message";

        
        this.company = args.company || null;

        
        var storage = Backendless.Persistence.of( Message );
        this.save = function ( async ) {
            cleanPrivateRelations(this);
            storage.save( this, async );
        };
        this._private_describeClass = function(){
            return Backendless.Persistence.describe(this.___class);
        };
        this.remove = function ( async ) {
            var result = storage.remove( this, async );
            this.objectId = null;
            return result;
        };
        
        this.getCompany = function ()
                    {
                    if( this.company == null )
                    storage.loadRelations( this, ['company'] );

                    return this.company;
        };
                
       }
       Reward: function Reward( args ){
        args = args || {};
        this._private_relations = [];
        this._private_geoRelations = [];
        this._private_dates = [
            "created",
            "updated"];
        this.___class = "Reward";

        
        var storage = Backendless.Persistence.of( Reward );
        this.save = function ( async ) {
            cleanPrivateRelations(this);
            storage.save( this, async );
        };
        this._private_describeClass = function(){
            return Backendless.Persistence.describe(this.___class);
        };
        this.remove = function ( async ) {
            var result = storage.remove( this, async );
            this.objectId = null;
            return result;
        };
        
       }
       Point: function Point( args ){
        args = args || {};
        this._private_relations = [
        "company"];
        this._private_geoRelations = [];
        this._private_dates = [
            "updated",
            "created"];
        this.___class = "Point";

        
        this.company = args.company || null;
        
        var storage = Backendless.Persistence.of( Point );
        this.save = function ( async ) {
            cleanPrivateRelations(this);
            storage.save( this, async );
        };
        this._private_describeClass = function(){
            return Backendless.Persistence.describe(this.___class);
        };
        this.remove = function ( async ) {
            var result = storage.remove( this, async );
            this.objectId = null;
            return result;
        };
        
        this.getCompany = function ()
                    {
                    if( this.company == null )
                    storage.loadRelations( this, ['company'] );

                    return this.company;
        };
                
       }
       Company: function Company( args ){
        args = args || {};
        this._private_relations = [
        "rewards",
        "promotions"];
        this._private_geoRelations = [];
        this._private_dates = [
            "created",
            "updated"];
        this.___class = "Company";

        
        this.rewards = args.rewards || null;
        
        this.promotions = args.promotions || null;
        
        var storage = Backendless.Persistence.of( Company );
        this.save = function ( async ) {
            cleanPrivateRelations(this);
            storage.save( this, async );
        };
        this._private_describeClass = function(){
            return Backendless.Persistence.describe(this.___class);
        };
        this.remove = function ( async ) {
            var result = storage.remove( this, async );
            this.objectId = null;
            return result;
        };
        
        this.addItemToRewards = function ( item )
                    {
                    if( this.rewards == null )
                        this.rewards = [];

                    this.rewards.push( item );
                    return this.rewards;
        };

        this.removeItemFromRewards = function ( item, async )
                    {
                    if( this.rewards == null )
                        storage.loadRelations( this, ['rewards'] );

                    for( var j = 0; j < this.rewards.length; j++ )
                    {
                    if( this.rewards[j].objectId === item.objectId )
                    {
                    this.rewards.splice( j, j + 1 );
                    this.save( async );
                    break;
                    }
                    }
        };
        this.removeAllRewards = function ( async )
                    {
                    this.rewards = null;
                    this.save( async );
        };

        this.getRewards = function ()
                    {
                    if( this.rewards == null )
                    storage.loadRelations( this, ['rewards'] );

                    return this.rewards;
        };
                
        this.addItemToPromotions = function ( item )
                    {
                    if( this.promotions == null )
                        this.promotions = [];

                    this.promotions.push( item );
                    return this.promotions;
        };

        this.removeItemFromPromotions = function ( item, async )
                    {
                    if( this.promotions == null )
                        storage.loadRelations( this, ['promotions'] );

                    for( var j = 0; j < this.promotions.length; j++ )
                    {
                    if( this.promotions[j].objectId === item.objectId )
                    {
                    this.promotions.splice( j, j + 1 );
                    this.save( async );
                    break;
                    }
                    }
        };
        this.removeAllPromotions = function ( async )
                    {
                    this.promotions = null;
                    this.save( async );
        };

        this.getPromotions = function ()
                    {
                    if( this.promotions == null )
                    storage.loadRelations( this, ['promotions'] );

                    return this.promotions;
        };
                
       }
       Promotion: function Promotion( args ){
        args = args || {};
        this._private_relations = [];
        this._private_geoRelations = [];
        this._private_dates = [
            "updated",
            "created"];
        this.___class = "Promotion";

        
        var storage = Backendless.Persistence.of( Promotion );
        this.save = function ( async ) {
            cleanPrivateRelations(this);
            storage.save( this, async );
        };
        this._private_describeClass = function(){
            return Backendless.Persistence.describe(this.___class);
        };
        this.remove = function ( async ) {
            var result = storage.remove( this, async );
            this.objectId = null;
            return result;
        };
        
       }
                            