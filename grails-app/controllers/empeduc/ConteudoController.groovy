package empeduc

class ConteudoController {

    def index() { }
    
    def processo_empreendedor() { }
    
    def GameEdu() { }
    
    def empreendedorismo_brasil() {}
    
    def empreendedorismo_evolucao (){}
    
    def mitos_e_verdade_empreendimento (){}
    
    def tipos_empreendedores (){}
    
    def conteudos () {
        redirect (uri: "")
    }
    
}
