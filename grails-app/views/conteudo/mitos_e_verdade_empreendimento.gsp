<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>Webarch - Responsive Admin Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
    
<!-- BEGIN PLUGIN CSS -->
 <link href="${resource(dir: 'template', file:'plugins/pace/pace-theme-flash.css')}" rel="stylesheet" type="text/css" media="screen"/>
<!-- END PLUGIN CSS -->
<!-- BEGIN CORE CSS FRAMEWORK -->
 <link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap.min.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap-theme.min.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'plugins/font-awesome/css/font-awesome.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'css/animate.min.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.css')}" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
 <link href="${resource(dir: 'template', file:'css/style.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'css/responsive.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'css/custom-icon-set.css')}" rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
	<div class="header-seperation"> 
		<ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
		 <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
		</ul>
      <!-- BEGIN LOGO -->	
      <a href="index.html"><img src="${resource(dir: 'template', file:'/img/logo.png')}" class="logo" alt=""  data-src="${resource(dir: 'template', file:'/img/logo.png')}" data-src-retina="assets/img/logo2x.png')}" width="106" height="21"/></a>
      <!-- END LOGO --> 
      <ul class="nav pull-right notifcation-center">	
        <li class="dropdown" id="header_task_bar"> <a href="index.html" class="dropdown-toggle active" data-toggle=""> <div class="iconset top-home"></div> </a> </li>
        <li class="dropdown" id="header_inbox_bar" > <a href="email.html" class="dropdown-toggle" > <div class="iconset top-messages"></div>  <span class="badge" id="msgs-badge">2</span> </a></li>
		<li class="dropdown" id="portrait-chat-toggler" style="display:none"> <a href="#sidr" class="chat-menu-toggle"> <div class="iconset top-chat-white "></div> </a> </li>        
      </ul>
      </div>
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav" > 
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu"> 
  <!-- BEGIN MINI-PROFILE -->
	   <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
		   <div class="user-info-wrapper">	
				<div class="profile-wrapper">
					<img src="${resource(dir: 'template', file:'/img/aluno.png')}"  alt="" data-src="${resource(dir: 'template', file:'/img/profiles/avatar.jpg')}" data-src-retina="assets/img/profiles/avatar2x.jpg')}" width="69" height="69" />
				</div>
				<div class="user-info">
				  <div class="greeting">Bem-vindo,</div>
				  <div class="username">Daniel <span class="semi-bold">Enos</span></div>
				  <div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div>
				</div>
		   </div>
	  <!-- END MINI-PROFILE -->
	   
	   <!-- BEGIN SIDEBAR MENU -->	
		<ul>	
		  <li class=""> <a href="../frontend/index.html"> <i class="fa fa-flag"></i>  <span class="title">Perfil</span></a></li>

		  <li class=""> <a href="../frontend/index.html"> <i class="fa fa-flag"></i>  <span class="title">Conteúdos</span></a></li>
				
		  <li class="active open "> <a href="javascript:;"> <i class="fa fa fa-adjust"></i> <span class="title">Fórum</span> <span class="arrow open"></span> </a>
            <ul class="sub-menu">
              <li > <a id="botao_promocao_criar" >Minhas Mensagens</a> </li>
              <li > <a id="botao_promocao_editar">Mensagens do Professor </a> </li>
			  <li > <a id="botao_promocao_premiar">Premiar </a> </li>
            </ul>
		  </li>  		
		<li class=""> <a href="../frontend/index.html"> <i class="fa fa-flag"></i>  <span class="title">Sair</span></a></li> 
		</ul>	
		<!-- END SIDEBAR MENU --> 
	  </div>
  </div>
  <!-- END SIDEBAR --> 
  
  
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content"> 
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">  
		<div class="page-title">	
			<h3>Mitos e verdades do <span class="semi-bold">Emp</span>reendedorismo</h3>	
			
			
<div align = "justify">	
<p>No processo de desenvolvimento das características empreendedoras, algumas pessoas que ainda não possuem o conhecimento sobre os desafios de ser um empreendedor podem acreditar inicialmente que ser dono do próprio negócio é uma atividade profissional mais fácil que outras presentes no mercado.</p>
<p>Como forma de refletirmos sobre alguns mitos e verdades relacionadas ao empreendedorismo criamos um pequeno quiz com questões onde será possível marcar Verdadeiro ou Falso e desmistificar algumas ideias relacionadas à vida do empreendedor.</p>
<p>Vamos Jogar?


</br></br></br></br>
<div align="center">
	<b style="font-size: 18px" id="questao">Questão: </b>
	<a style="font-size: 18px" id="pergunta">Pergunta</a>
	</br></br>
	<a href="#" style="font-size: 15px" id="verdadeiro">Verdadeiro</a>
	</br>
	<a href="#" style="font-size: 15px" id="falso"> Falso </a>
	</br>
	<b id="comentario" style="font-size: 14px">Comentários: </b>
	<a style="font-size: 14px" id="correcao">Correção</a>
	</br></br>
	<button id="proximo"> Próximo </button>
	</br></br>
	
	<p><h4>REFERÊNCIAS</h4></p>
	<div align="justify">
		<p>DORNELAS, José Carlos Assis. Empreendedorismo na prática: mitos e verdades do empreendedor de sucesso.1. ed. Rio de Janeiro: Elsevier, 2007.</p>
		<p>DORNELAS, José Carlos de Assis. Empreendedorismo: transformando ideias em negócios. 5. ed. Rio de Janeiro: Empreende/LTC, 2014.</p>
		<p>Serviço de Apoio às Micro e Pequenas Empresas de São Paulo – SEBRAE/SP. Empreendedorismo: SEBRAE-SP. 2011. Disponível <www.sebraesp.com.br>.</p>
	</div>
	
</div>
</div>		
			
			
			
		</div>
    </div>
  </div>  
 <!-- END CONTAINER --> 
</div>
<!-- END CONTAINER -->

<!-- BEGIN CORE JS FRAMEWORK--> 
<script src="${resource(dir: 'template', file:'/plugins/jquery-1.8.3.min.js" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'/plugins/breakpoints.js" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<!-- END CORE JS FRAMEWORK --> 
<!-- BEGIN PAGE LEVEL JS --> 
<script src="${resource(dir: 'template', file:'/plugins/pace/pace.min.js" type="text/javascript"></script>  
<script src="${resource(dir: 'template', file:'/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS --> 	

<!-- BEGIN CORE TEMPLATE JS --> 
<script src="${resource(dir: 'template', file:'/js/core.js" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'/js/chat.js" type="text/javascript"></script> 
<!-- END CORE TEMPLATE JS --> 
<script>
	$(document).ready(function(){
		function setPergunta(codigo,questao,proximo,anterior,verdadeiro,falso,elemento2,texto){
				$(elemento2).text(texto);
				$(elemento2).show();
				$(questao).show();
				$(proximo).show();
				$(anterior).show();
				$(verdadeiro).show();
				$(falso).show();
				return codigo;
		}
		
		function setCorrecao(codigo,verdadeiro,verdadeiroCor,falso,falsoCor,correcao,textoCorrecao,comentario){
			$(verdadeiro).click(function(){
				$(verdadeiro).css("color", verdadeiroCor);
				$(falso).css("color", falsoCor);
				$(correcao).show();
				$(correcao).text(textoCorrecao);
				$(comentario).show();
			});
			
			$(falso).click(function(){
				$(verdadeiro).css("color", verdadeiroCor);
				$(falso).css("color", falsoCor);
				$(correcao).show();
				$(correcao).text(textoCorrecao);
				$(comentario).show();
			});
			return codigo;
		}
		
		$("#questao").hide();
		$("#pergunta").hide();
		$("#verdadeiro").hide();
		$("#falso").hide();
		$("#correcao").hide();
		$("#proximo").hide();
		$("#anterior").hide();
		$("#comentario").hide();
		
		setPergunta(1,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta","“Somente a sorte e a criatividade fazem um empreendedor de sucesso”.");
		setCorrecao(1,"#verdadeiro","red","#falso","green","#correcao","“Um grande mito é que para ser empreendedor bem-sucedido é necessário somente de sorte e criatividade, porém, conforme Dornelas (2007) na verdade para se dar bem no processo empreendedor é necessário ter muita habilidade e conhecimento na área. ”","#comentario");
		
		var contador=1;
		
		$("#proximo").click(function(){
	
			if(contador == 1){
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").css("color", "blue");
				$("#falso").css("color", "blue");
				setPergunta(2,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta","“Empreendedores assumem riscos calculados”.");
				setCorrecao(2,"#verdadeiro","green","#falso","red","#correcao","“Grandes empreendedores conquistaram o sucesso assumindo riscos calculados, eles planejam bem os riscos e identificam as formas de minimizar os possíveis danos.”","#comentario");
				contador++;
			}else if(contador == 2){
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").css("color", "blue");
				$("#falso").css("color", "blue");
				setPergunta(3,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta","“O empreendedor trabalha menos e ganha mais dinheiro”");
				setCorrecao(3,"#verdadeiro","red","#falso","green","#correcao","“De acordo com o Sebrae/SP (2011), depois da empresa criada o empreendedor se sacrifica, sendo as vezes colocando seus próprios bens pessoais em risco de perda, como em garantias de empréstimos, começa a trabalha e gerenciar pessoas e começar a surgir problemas para resolver.”","#comentario");
				contador++;
			}else if(contador == 3){
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").css("color", "blue");
				$("#falso").css("color", "blue");
				setPergunta(4,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta","“Capital é o principal elemento para ter um negócio de sucesso”.");
				setCorrecao(4,"#verdadeiro","red","#falso","green","#correcao","“O dinheiro não é o principal elemento para se construir um negócio, o principal elemento é um empreendedor preparado, o dinheiro será um facilitador, quando utilizadas pelas mãos certas.","#comentario");
				contador++;
			}else if(contador == 4){
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").css("color", "blue");
				$("#falso").css("color", "blue");
				setPergunta(5,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta","“Empreendedores formam equipes e são grandes líderes”");
				setCorrecao(5,"#verdadeiro","green","#falso","red","#correcao","“O trabalho em equipe e a forma de liderança da equipe são fatores muito importes para o empreendedor atingir suas metas. Os empreendedores não trabalham sozinhos, é muito importante ter uma equipe e sócios para dividir as responsabilidades. ”","#comentario");
				contador++;
			}else if(contador == 5){
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").css("color", "blue");
				$("#falso").css("color", "blue");
				setPergunta(6,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta","“Empreendedores nascem prontos”.");
				setCorrecao(6,"#verdadeiro","red","#falso","green","#correcao","“Muitas pessoas nascem com características empreendedoras, porém para se tornar um grande empreendedor é preciso estudar e adquirir conhecimento na área de atuação do negócio, como técnicas de gerenciamento, por exemplo. Quem não nasce com essas características pode se tornar um grande empreendedor, com prática, estudos e conhecimentos de habilidades sobre o empreendedorismo e formas de gerenciar o negócio.”","#comentario");
				contador++;
			}else if(contador == 6){
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").css("color", "blue");
				$("#falso").css("color", "blue");
				setPergunta(7,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta","“Empreendedores são muito organizados”");
				setCorrecao(7,"#verdadeiro","green","#falso","red","#correcao","“Segundo Dornelas (2014), os empreendedores são organizados, pois necessitam distribuir de forma racional os recursos, comandar a equipe e controlar o negócio para seu conseguir o máximo desempenho. ”","#comentario");
				contador++;
			}else if(contador == 7){
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").css("color", "blue");
				$("#falso").css("color", "blue");
				setPergunta(8,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta","“Empreendedores são jovens e cheios de energia”.");
				setCorrecao(8,"#verdadeiro","red","#falso","green","#correcao","“Os empreendedores podem estar em qualquer idade, a oportunidade pode aparecer a qualquer momento.”","#comentario");
				contador++;
			}else if(contador == 8){
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").css("color", "blue");
				$("#falso").css("color", "blue");
				setPergunta(9,"#questao","#proximo","#anterior","#verdadeiro","#falso","#pergunta",": “Os parceiros estratégicos são de grande importação para a sobrevivência do negócio.”");
				setCorrecao(9,"#verdadeiro","green","#falso","red","#correcao","“De acordo com Dornelas (2007), a rede de parceiros, ou networking, é essencial para o negócio, ela servira como apoio em momentos de dificuldades ou como ajuda em relação a experiência no mercado”.","#comentario");
				contador++;
			}else{
				$("#comentario").hide();
				$("#correcao").hide();
				$("#verdadeiro").hide();
				$("#falso").hide();
				$("#questao").hide();
				$("#pergunta").text("Fim do Jogo!");
				$("#proximo").hide();
			}
		
		});
		
	});
</script>
</body>
</html>