package empeduc

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class DiarioController {

    DiarioService diarioService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond diarioService.list(params), model:[diarioCount: diarioService.count()]
    }

    def show(Long id) {
        respond diarioService.get(id)
    }

    def create() {
        respond new Diario(params)
    }

    def save(Diario diario) {
        if (diario == null) {
            notFound()
            return
        }

        try {
            diarioService.save(diario)
        } catch (ValidationException e) {
            respond diario.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'diario.label', default: 'Diario'), diario.id])
                redirect diario
            }
            '*' { respond diario, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond diarioService.get(id)
    }

    def update(Diario diario) {
        if (diario == null) {
            notFound()
            return
        }

        try {
            diarioService.save(diario)
        } catch (ValidationException e) {
            respond diario.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'diario.label', default: 'Diario'), diario.id])
                redirect diario
            }
            '*'{ respond diario, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        diarioService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'diario.label', default: 'Diario'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'diario.label', default: 'Diario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
