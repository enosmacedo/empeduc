<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>Webarch - Responsive Admin Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
    
<!-- BEGIN PLUGIN CSS -->
<link href="${resource(dir: 'template', file:'plugins/pace/pace-theme-flash.css')}"  rel="stylesheet" type="text/css" media="screen"/>
<!-- END PLUGIN CSS -->
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap-theme.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/font-awesome/css/font-awesome.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/animate.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.css')}"  rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link href="${resource(dir: 'template', file:'css/style.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/responsive.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/custom-icon-set.css')}"  rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
	<div class="header-seperation"> 
		<ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
		 <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
		</ul>
      <!-- BEGIN LOGO -->	
      <a href="index.html"><img src="${resource(dir: 'template', file:'/img/logo.png')}" class="logo" alt=""  data-src="${resource(dir: 'template', file:'/img/logo.png')}" data-src-retina="assets/img/logo2x.png')}" width="106" height="21"/></a>
      <!-- END LOGO --> 
      <ul class="nav pull-right notifcation-center">	
        <li class="dropdown" id="header_task_bar"> <a href="index.html" class="dropdown-toggle active" data-toggle=""> <div class="iconset top-home"></div> </a> </li>
        <li class="dropdown" id="header_inbox_bar" > <a href="email.html" class="dropdown-toggle" > <div class="iconset top-messages"></div>  <span class="badge" id="msgs-badge">2</span> </a></li>
		<li class="dropdown" id="portrait-chat-toggler" style="display:none"> <a href="#sidr" class="chat-menu-toggle"> <div class="iconset top-chat-white "></div> </a> </li>        
      </ul>
      </div>
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav" > 
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu"> 
  <!-- BEGIN MINI-PROFILE -->
	   <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
		   <div class="user-info-wrapper">	
				<div class="profile-wrapper">
					<img src="${resource(dir: 'template', file:'/img/aluno.png')}"  alt="" data-src="${resource(dir: 'template', file:'/img/profiles/avatar.jpg')}" data-src-retina="assets/img/profiles/avatar2x.jpg')}" width="69" height="69" />
				</div>
				<div class="user-info">
				  <div class="greeting">Bem-vindo,</div>
				  <div class="username">Daniel <span class="semi-bold">Enos</span></div>
				  <div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div>
				</div>
		   </div>
	  <!-- END MINI-PROFILE -->
	   
	   <!-- BEGIN SIDEBAR MENU -->	
		<ul>	
		  <li class=""> <a href="/"> <i class="fa fa-flag"></i>  <span class="title">Perfil</span></a></li>

		  <li class=""> <a href="/"> <i class="fa fa-flag"></i>  <span class="title">Conteúdos</span></a></li>
				
		  <li class="active open "> <a href="javascript:;"> <i class="fa fa fa-adjust"></i> <span class="title">Fórum</span> <span class="arrow open"></span> </a>
            <ul class="sub-menu">
              <li > <a id="botao_promocao_criar" >Minhas Mensagens</a> </li>
              <li > <a id="botao_promocao_editar">Mensagens do Professor </a> </li>
			  <li > <a id="botao_promocao_premiar">Premiar </a> </li>
            </ul>
		  </li>  		
		  <li class=""> <a href="${resource(file: '../areaRestrita/login')}"> <i class="fa fa-flag"></i>  <span class="title">Sair</span></a></li>
		</ul>	
		<!-- END SIDEBAR MENU --> 
	  </div>
  </div>
  <!-- END SIDEBAR --> 
  
  
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content"> 
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">  
		<div align = "justify"  class="page-title">	
			<h3>Processo <span class="semi-bold">Emp</span>reendedor</h3>	

			<p>Vocês já pararam pra pensar quais são os erros mais comuns entre os empreendedores?.</p> 
			<p>Certamente, você responderá questões ligadas a fase de implantação ou de gerenciamento do negócio. Pois bem, saibam que também podemos incluir nesse conjunto de falhas o desconhecimento do chamado Processo Empreendedor.</p>
			<p>Para começarmos um negócio e nos tornarmos um empreendedor de sucesso, é necessário muito mais que o talento, precisamos nos qualificar e termos uma ótima equipe para nos ajudar. O empreendedor poderá encontrar dificuldades em todas as fases de implantação e gerenciamento do seu negócio, pois não adianta ser talentoso e possuir uma ideia inovadora, mas não conseguir colocar sua ideia em prática.</p>
			<p>Conforme Dornelas (2014), o Processo Empreendedor pode ser dividido em 4 fases, a saber: identificar e avaliar a oportunidade; desenvolver o plano de negócios; determinar e captar os recursos necessários; gerenciar a empresa criada. O processo não acontecerá nessa ordem sempre, mas elas sempre estarão interligadas.</p>
			
			<div align="center">
				<img src="${resource(dir: 'template', file:'img/processo_emp1.png')}" width="60%" height="420px"/>
				<p>Figura 01: As 4 (quatro) fases do Processo Empreendedor</p>
				<p>Fonte: Adaptado de Dornelas (2014, p. 31). </p>
			</div>	
			<p>Ainda conforme o autor, a fase de Identificar e avaliar a oportunidade é o coração do processo, pois nela o empreendedor terá que aproveitar a oportunidade e juntar seus talentos e conhecimentos para poder seguir em frente. Nesta fase ainda avalia os riscos e todos os benefícios da sua ideia, analisa competidores e verifica pontos positivos da oportunidade. </p>
			<p>Mas antes de prosseguir pare para pensar: Será que existe diferença entre a ideia e uma oportunidade?. </p>
			<p>Sobre essa questão, Dornelas (2014) defende que uma ideia não tem compromisso econômico, muitas ideias são inviáveis quando começa a se analisar, porém a oportunidade é uma ideia economicamente viável. Muitas vezes as grandes ideais iniciais são inviáveis, por isso, ao invés de desistir da ideia é necessário lapidá-las, transformando em uma grande oportunidade.</p>
			<p>Para o autor, nessa fase é comum o empreendedor cometer erros iniciais graves. A falta de paciência e a ansiedade por resultados são os grandes inimigos de empreendedores. Assim, para poder aperfeiçoar uma ideia é preciso tempo e calma para fazer a análise dos riscos, do mercado e os pontos inviáveis da ideia inicial e melhorá-la.</p>
			<p>Para verificar se realmente uma ideia é viável e pode se configurar como uma oportunidade, Dornelas (2014) apresentou roteiro simples conhecido como 3M’s: Market Demand (Demanda de Mercado), Market Structure and Size (Tamanho e Estrutura do Mercado) e Margin Analysis (Analise de Margem).</p>
			
			<div align="center">
				<img src="${resource(dir: 'template', file:'/img/processo_emp2.png')}" width="60%" height="420px"/>
				<p>Figura 02: 3M’S</p>
				<p>Fonte: Adaptado de Dornelas (2014).</p>
			</div>	

			<p>O primeiro M, Demanda de Mercado, diz respeito a visibilidade do produto ou ideia tem no mercado, como sua durabilidade, se mercado tem um bom potencial de crescimento e se o cliente alvo tem um bom acesso aos produtos ou serviços.</p>
			<p>O segundo M, Tamanho e Estrutura do Mercado, trata de fatores que influencia o comportamento do produto/ serviço no mercado atual, como o estágio do ciclo de vida que ele se encontra, a análise de competidores e fornecedores e também</p> identificar as barreiras e dificuldades que ele enfrentará e formas de superar essas barreiras.
			<p>A última parte do roteiro, o terceiro M, Analise de Margem, irá se preocupar com o valor do produto/serviço, como os custos iniciais, as formas de lucrar e o tempo de retorno do investimento. </p>
			<p>Uma boa dica de filme para aqueles que gostam de compreender como a questão dos riscos podem influenciar na nossa vida é o filme “The Pursuit of Happyness” – À Procura da Felicidade, como é conhecido no Brasil. A película, baseada em fatos reais, conta a história de Chris Gardner (Will Smith) e seu filho Christopher (Jaden Smith), que lutam para ter uma vida melhor. Para saber mais, veja o trailer do filme:</br> <a href="https://www.youtube.com/watch?v=6yc069E1gfc&oref=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D6yc069E1gfc&has_verified=1">https://www.youtube.com/watch?v=6yc069E1gfc&oref=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D6yc069E1gfc&has_verified=1</a></p>

			<p>Logo depois da oportunidade aparecer é preciso desenvolver a ideia e transformá-la em um negócio. Para isso, as ideias precisam ser colocadas no papel, o que entendemos como a fase de criação do plano de negócios. Nesse plano deve ser colocado todas as caraterísticas da empresa de forma bem detalhada, explicando todo seu negócio, os produtos, serviços, analise de mercado, a estratégia inicial e também o plano financeiro da empresa. Esse plano será de muita importância para a próxima fase, captar os recursos financeiros necessários (DORNELAS, 2014).</p>
			<p>Ainda conforme o autor, para colocar o finalmente negócio em prática, é preciso conseguir os recursos necessários para realizar o investimento. Na busca por um investimento inicial o empreendedor utiliza de recursos pessoais ou empréstimos de amigos e parentes para começar o negócio, porém é preciso ter controle e consciência para não entrar em dividas e acabar com o negócio. </p>
			<p>Outra forma de conseguir dinheiro é com investimentos de empresários e empreendedores já bem-sucedidos, um exemplo desse tipo investidor é o angel investor (investidor-anjo) – pessoa física que analisa o novo negócio e seu potencial e decide fazer um investimento inicial para impulsionar a empresa, em troca, muitos querem uma parte do controle acionário da empresa. Existe ainda outros tipos de investidores, como aqueles que começam a investir depois que a empresa começa a crescer.</p>
			<p>Por fim, tem-se a fase de gerenciamento do negócio, onde o empreendedor, juntamente com a equipe empresarial, irá comandar a empresa e fazer com que ela cumpra os objetivos propostos no plano de negócios. Nesta fase o empreendedor utiliza todo seu talento, conhecimento e seu estilo de gerenciar. É importante que o empreendedor cresça junto com a empresa, pois aparecerá problemas e é necessário está sempre preparado.</p>
			<p>A equipe gerencial, os sócios e ajuda dos investidores já experientes faz uma diferença grande na hora de lidar com os problemas, por isso é crucial ter uma equipe excelente ao lado para ajudar, principalmente com empreendedores de primeira viagem, a ajuda de pessoas experientes são sempre bem-vindas.   </p>
			<p>Outra visão do chamado processo empreendedor é fornecida por Jeffry Timmons, que dividiu esse processo em 3 pilares: Oportunidade, Recursos e Equipe.
			 
			<div align="center">
				<img src="${resource(dir: 'template', file:'/img/processo_emp3.png')}" width="60%" height="420px"/>
				<p>Figura 03: Modelo de Timmons.</p>
				<p>Fonte: Adaptado de Dornelas (2014, p. 32).</p> 
			</div>	

			<p>Elas juntas seria o essencial para o empreendedor manter seu negócio de sucesso, elas não necessitam ocorrer em uma ordem, e junto com esses pilares, existe fatores que contribuiriam, como o conhecimento do empreendedor, o mercado externo favorável ou não, entre outros fatores sociais.</p>



			Referências
			DORNELAS, Jose Carlos de Assis. Empreendedorismo: transformando ideias em negócios. 5 ed. Rio de Janeiro: Empreende / LTC, 2014.

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		</div>
    </div>
  </div>  
 <!-- END CONTAINER --> 
</div>
<!-- END CONTAINER -->

<!-- BEGIN CORE JS FRAMEWORK--> 
<script src="${resource(dir: 'template', file:'plugins/jquery-1.8.3.min.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/boostrapv3/js/bootstrap.min.js"')}"type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/breakpoints.js" type="text/javascript')}"></script> 
<script src="${resource(dir: 'template', file:'plugins/jquery-unveil/jquery.unveil.min.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.min.js')}" type="text/javascript"></script>
<!-- END CORE JS FRAMEWORK --> 
<!-- BEGIN PAGE LEVEL JS --> 
<script src="${resource(dir: 'template', file:'plugins/pace/pace.min.js" type="text/javascript')}"></script>  
<script src="${resource(dir: 'template', file:'plugins/jquery-numberAnimate/jquery.animateNumbers.js')}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS --> 	

<!-- BEGIN CORE TEMPLATE JS --> 
<script src="${resource(dir: 'template', file:'js/core.js" type="text/javascript')}"></script> 
<script src="${resource(dir: 'template', file:'js/chat.js" type="text/javascript')}"></script> 
<!-- END CORE TEMPLATE JS --> 
</body>
</html>