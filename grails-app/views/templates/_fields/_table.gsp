<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sample title</title>
    </head>
    <body>
        <table class="table table-hover no-more-tables" style="background-color: white">
            <thead>
                 <tr>
                    <g:each in="${domainProperties}" var="p" status="i">
                        <g:sortableColumn property="${p.name}" title="${p.name}" />
                    </g:each>
                </tr>
            </thead>
            <tbody>
                <g:each in="${collection}" var="bean" status="i">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                        <g:each in="${domainProperties}" var="p" status="j">
                            <g:if test="${j==0}">
                                <td><g:link method="GET" resource="${bean}"><f:display bean="${bean}" property="${p.name}" displayStyle="${displayStyle?:'table'}" theme="${theme}"/></g:link></td>
                            </g:if>
                            <g:else>
                                <td><f:display bean="${bean}" property="${p.name}"  displayStyle="${displayStyle?:'table'}" theme="${theme}"/></td>
                            </g:else>
                        </g:each>
                    </tr>
                </g:each>
            </tbody>
        </table>
    </body>
</html>
