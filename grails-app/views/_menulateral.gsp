<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->


  <!-- BEGIN CONTAINER -->
        <div class="page-container row-fluid">
          <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar" id="main-menu"> 
            <!-- BEGIN MINI-PROFILE -->
                <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
                    <div class="user-info-wrapper">	
                        <div class="profile-wrapper">
                            <img src="assets/img/aluno.png"  alt="" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" width="69" height="69" />-->
                        </div>
                        <div class="user-info">
                            <div class="greeting">Bem-vindo,</div>
                            <div class="username">Daniel <span class="semi-bold">Enos</span></div>
                            <div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div>
                        </div>
                    </div>
           <!-- END MINI-PROFILE -->
                    <!-- BEGIN SIDEBAR MENU -->	
                    <ul>	
                        <sec:ifAnyGranted roles='ROLE_ALUNO'>
                            <li class=""> <g:link controller="Aluno" action="perfil"> <i class="fa fa-flag"></i>  <span class="title">Perfil</span></g:link></li>
                        </sec:ifAnyGranted>
                        <sec:ifAnyGranted roles='ROLE_PROFESSOR'>
                            <li class=""> <g:link controller="Professor" action="perfil"> <i class="fa fa-flag"></i>  <span class="title">Perfil</span></g:link></li>
                        </sec:ifAnyGranted>
                        <sec:ifAnyGranted roles='ROLE_DIRETOR'>
                            <li class=""> <g:link controller="Diretor" action="perfil"> <i class="fa fa-flag"></i>  <span class="title">Perfil</span></g:link></li>
                        </sec:ifAnyGranted>
                        <li class=""> <a href="conteudo/conteudos"> <i class="fa fa-flag"></i>  <span class="title">Conteúdos</span></a></li>

                        <li class=" "> <a href="${resource(file: '../forum/index')}"> <i class="fa fa fa-adjust"></i> <span class="title">Fórum</span> </a></li>  		
                        <li class=""> <a href="${resource(file: '../areaRestrita/login')}"> <i class="fa fa-flag"></i>  <span class="title">Sair</span></a></li>
                    </ul>	
                    <!-- END SIDEBAR MENU --> 
                </div>
            </div>
            <!-- END SIDEBAR --> 