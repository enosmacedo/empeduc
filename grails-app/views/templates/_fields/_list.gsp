<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
    </head>
    <body>
        
        <table class="table table-hover no-more-tables" style="background-color: white">
            <g:each in="${domainClass.persistentProperties}" var="p">
                <g:if test="${p.name=='version' || p.name=='comentarios'}">
                </g:if>

                <g:else>

                       <tr>
                            <th> <span id="${p.name}-label" class="property-label"><g:message code="${domainClass}.${p.name}.label" default="${p.name}" /></span> </th>
                            <td> <div class="property-value" aria-labelledby="${p.name}-label">${body(p)}</div> </td>
                       </tr>

                </g:else>

            </g:each>
       
        </table>
        
        
    </body>
</html>
