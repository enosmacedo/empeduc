<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" contet="width=device-width, inicial-scale=1.0">
        
        
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>EMPEDUC</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN CORE CSS FRAMEWORK -->
        <link href="${resource(dir: 'template', file:'/plugins/pace/pace-theme-flash.css')}" rel="stylesheet" type="text/css" media="screen"/>
        <link href="${resource(dir: 'template', file:'/plugins/boostrapv3/css/bootstrap.min.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/plugins/boostrapv3/css/bootstrap-theme.min.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/plugins/font-awesome/css/font-awesome.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/animate.min.css" rel="stylesheet')}" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/plugins/jquery-scrollbar/jquery.scrollbar.css')}" rel="stylesheet" type="text/css"/>
        <!-- END CORE CSS FRAMEWORK -->
        <!-- BEGIN CSS TEMPLATE -->
        <link href="${resource(dir: 'template', file:'/css/style.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/responsive.css')}" rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/custom-icon-set.css')}" rel="stylesheet" type="text/css"/>
        
        <g:set var="entityName" value="${message(code: 'forum.label', default: 'Forum')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body class="">
    <!-- BEGIN HEADER -->
        <div class="header navbar navbar-inverse "> 
          <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="navbar-inner">
                <div class="header-seperation"> 
                    <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
                        <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
                    </ul>

                </div>
                <!-- END RESPONSIVE MENU TOGGLER --> 
                <div class="header-quick-nav" > 
                    <div class="pull-right"> 
                        <div class="chat-toggler">	
                            <div class="profile-pic"> 
                                <img src="/assets/img/profiles/avatar_small.jpg"  alt="" data-src="/assets/img/profiles/avatar_small.jpg" data-src-retina="/assets/img/profiles/avatar_small2x.jpg" width="35" height="35" /> 
                            </div>
                            <div class="talkballon">
                                <div class="talk-bubble tri-right right-in">
                                    <div class="talktext">
                                        <p id="talkballon">Teste</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- END TOP NAVIGATION MENU --> 
            </div>
            <!-- END TOP NAVIGATION BAR --> 
        </div>
        <!-- END HEADER -->


        <!-- BEGIN CONTAINER -->
        <g:render template="../menulateral" />
                          
        <br>
                          
        <!-- BEGIN PAGE CONTAINER-->
          <div class="page-content"> 
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="content">  
        
                          
        <div class="nav" role="navigation">
            <ul style="list-style:none">
                <li style="display:inline"><a class="btn btn-primary btn-cons" href="${createLink(uri: '/')}"><i class="fa fa-home">&nbsp</i><g:message code="default.home.label"/></a></li>
                <li style="display:inline"><g:link class="btn btn-primary btn-cons" action="index"><i class="fa fa-list-alt">&nbsp</i><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        
        <br><br>
        
        <div>
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.forum}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.forum}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.forum}" method="POST">
                <fieldset class="form">
                    <f:all bean="forum"/>
                </fieldset>
                <fieldset class="buttons">
                    
                    <br>
                    
                    <g:submitButton name="create" class="btn btn-primary btn-cons" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </g:form>
        </div>
        
       
        </div>
            </div>  
           <!-- END CONTAINER --> 
          </div>
          <!-- END CONTAINER -->
        
        
    <!-- BEGIN CORE JS FRAMEWORK--> 
          <script src="${resource(dir: 'template', file:'plugins/jquery-1.8.3.min.js')}" type="text/javascript"></script> 
          <script src="${resource(dir: 'template', file:'plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js')}" type="text/javascript"></script> 
          <script src="${resource(dir: 'template', file:'plugins/boostrapv3/js/bootstrap.min.js"')}"type="text/javascript"></script> 
          <script src="${resource(dir: 'template', file:'plugins/breakpoints.js')}" type="text/javascript"></script> 
          <script src="${resource(dir: 'template', file:'plugins/jquery-unveil/jquery.unveil.min.js')}" type="text/javascript"></script> 
          <script src="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.min.js')}" type="text/javascript"></script>
          <!-- END CORE JS FRAMEWORK --> 
          <!-- BEGIN PAGE LEVEL JS --> 
          <script src="${resource(dir: 'template', file:'plugins/pace/pace.min.js')}" type="text/javascript"></script>  
          <script src="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.min.js')}" type="text/javascript"></script>
          <script src="${resource(dir: 'template', file:'plugins/jquery-block-ui/jqueryblockui.js"')}" type="text/javascript"></script>
          <script src="${resource(dir: 'template', file:'plugins/jquery-sparkline/jquery-sparkline.js')}" type="text/javascript"></script>
          <script src="${resource(dir: 'template', file:'plugins/jquery-numberAnimate/jquery.animateNumbers.js')}" type="text/javascript"></script>
          <!-- END PAGE LEVEL PLUGINS --> 	

          <!-- BEGIN CORE TEMPLATE JS --> 
          <script src="${resource(dir: 'template', file:'js/core.js" type="text/javascript')}"></script> 
          <script src="${resource(dir: 'template', file:'js/chat.js" type="text/javascript')}"></script>
          <script src="${resource(dir: 'template', file:'js/demo.js" type="text/javascript')}"></script>
          <!-- END CORE TEMPLATE JS --> 
        
    </body>
</html>
