package empeduc

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

import comum.Permissao
import comum.Usuario
import comum.UsuarioPermissao

class AlunoController {
    
    def springSecurityService
    
    AlunoService alunoService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        
        params.max = Math.min(max ?: 10, 100)
        respond alunoService.list(params), model:[alunoCount: alunoService.count()]
    }
    
    def perfil(Long id){
        
        String a = alunoService.list(params)
        String aux = a.charAt(a.length()-2)
        
        int i = 2
        
        while(true){
            if(alunoService.get(aux).email == springSecurityService.principal.username){
                int teste = Integer.parseInt(aux)
                id = teste
                break
            }
            else{
                i+=19
                aux = a.charAt(a.length()-i)
                
            }
             
        }
        
        redirect (uri : '/aluno/show/' + id)        
    }

    def show(Long id) {
        
        Permissao aluno = Permissao.findByAuthority("ROLE_ALUNO")
        if (aluno == null) {
            aluno = new Permissao (authority: "ROLE_ALUNO").save(flush: true)
        }
        
        Usuario aluno1 = Usuario.findByUsername(alunoService.get(id).email)
        if (aluno1 == null) {
            aluno1 = new Usuario(username: alunoService.get(id).email, password: alunoService.get(id).senha, enabled:true,
                accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
        }
        
        if(UsuarioPermissao.findByUsuarioAndPermissao(aluno1, aluno) == null) {
            new UsuarioPermissao(usuario: aluno1, permissao: aluno).save(flush: true)
        }
        
        respond alunoService.get(id)
    }

    def create() {
        
        respond new Aluno(params)
    }

    def save(Aluno aluno) {
        if (aluno == null) {
            notFound()
            return
        }

        try {
            alunoService.save(aluno)
        } catch (ValidationException e) {
            respond aluno.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'aluno.label', default: 'Aluno'), aluno.id])
                redirect aluno
            }
            '*' { respond aluno, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond alunoService.get(id)
    }

    def update(Aluno aluno) {
        if (aluno == null) {
            notFound()
            return
        }

        try {
            alunoService.save(aluno)
        } catch (ValidationException e) {
            respond aluno.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'aluno.label', default: 'Aluno'), aluno.id])
                redirect aluno
            }
            '*'{ respond aluno, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        alunoService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'aluno.label', default: 'Aluno'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'aluno.label', default: 'Aluno'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
