function Company(args) {
    args = args || {};
    this.address = args.address || "";
    this.email = args.email || "";
    this.logo = args.logo || "";
    this.name = args.name || "";
    this.promotions = args.promotions || "";
}

function Promotion(args) {
    args = args || {};
    this.objectId = args.objectId || "";
    this.title = args.title || "";
    this.description = args.description || "";
    this.ownerld = args.ownerld || "";
	this.created = args.created || "";
	this.update = args.update || "";
}
