<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>EMPEDUC</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="${resource(dir: 'template', file:'assets/plugins/pace/pace-theme-flash.css')}" rel="stylesheet" type="text/css" media="screen"/>
<link href="${resource(dir: 'template', file:'assets/plugins/boostrapv3/css/bootstrap.min.css')}" rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'assets/plugins/boostrapv3/css/bootstrap-theme.min.css')}" rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'assets/plugins/font-awesome/css/font-awesome.css')}" rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'assets/css/animate.min.css')}" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link href="${resource(dir: 'template', file:'assets/css/style.css')}" rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'assets/css/responsive.css')}" rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'assets/css/custom-icon-set.css')}" rel="stylesheet" type="text/css"/>
<style>
	#recuperarSenha{
		margin-left: 200px;
		margin-top: 50px;
	}
	#emailRecuperar{
		margin-left: 200px;
	}
	#botaoRecuperar{
		margin-left: 200px;
	}
</style>

<script src="${resource(dir: 'template', file:'http://www.google.com/jsapi')}"></script>
<script src="${resource(dir: 'template', file:'https://www.gstatic.com/firebasejs/3.9.0/firebase.js')}" ></script>
<script src="${resource(dir: 'template', file:'http://www.google.com/jsapi')}"></script>

<script src="${resource(dir: 'template', file:'https://www.gstatic.com/firebasejs/4.5.0/firebase.js')}"></script>
<script src="${resource(dir: 'template', file:'https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js')}"></script>
<script src="${resource(dir: 'template', file:'https://www.gstatic.com/firebasejs/4.2.0/firebase-auth.js')}"></script>
<script src="${resource(dir: 'template', file:'https://www.gstatic.com/firebasejs/4.2.0/firebase-database.js')}"></script>
<script src="${resource(dir: 'template', file:'https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js')}"></script>



</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="error-body no-top">
    <div class="container">
      <div class="row login-container column-seperation">  
            <div class="col-md-5 col-md-offset-1">
              <h2>Entre no EMPEDUC</h2>
              <p>O EMPEDUC é gratuito e sempre será gratuito.</p>
              <br>

                      <!-- 
                       <button class="btn btn-block btn-info col-md-8" type="button">
                <span class="pull-left"><i class="icon-facebook"></i></span>
                <span class="bold">Login com Facebook</span> </button>
                       <button class="btn btn-block btn-success col-md-8" type="button">
                <span class="pull-left"><i class="icon-twitter"></i></span>
                <span class="bold">Login com Twitter</span>
                        </button>
                            -->
            </div>

            <div class="col-md-5 "> <br>

                    <div class="row">
              <div class="form-group col-md-10">
                <label class="form-label">E-mail</label>
                <span class="help"></span>
                <div class="controls">
                                    <div class="input-with-icon  right">                                       
                                            <i class=""></i>
                                            <input type="email" name="email" id="email" required="required" class="form-control">                                 
                                    </div>
                </div>
              </div>
              </div>

                     <div class="row">
                     <div class="form-group col-md-10">
                <label class="form-label">Nome</label>
                <div class="controls">
                                    <div class="input-with-icon  right">                                       
                                            <i class=""></i>
                                            <input type="text" name="nome" id="nome" required="required" class="form-control">                                 
                                    </div>
                </div>
              </div>
              </div>
                      <div class="row">
              <div class="form-group col-md-10">
                <label class="form-label">Senha</label>
                <span class="help"></span>
                <div class="controls">
                                    <div class="input-with-icon  right">                                       
                                            <i class=""></i>
                                            <input type="password" name="senha" id="senha" required="required" class="form-control">                                 
                                    </div>
                </div>
              </div>
              </div>

              <div class="row">
                <div class="col-md-10">
                  <button class="btn btn-primary btn-cons pull-right" onClick="cadastrar()" type="submit">Cadastrar</button>
                </div>
              </div>
            </div>

                    <div class="row">
                <div class="col-md-10">
                  <center><h3 id="recuperarSenha">Esqueceu a senha? Recupere-a!</h3></center>
                              <center id="emailRecuperar"><label class="form-label">E-mail</label><center>
                              <span class="help"></span>
                              <div class="controls">
                                    <div class="input-with-icon  right">                                       
                                            <i class=""></i>
                                            <input type="email" name="recuperaSenha" id="recuperaSenha" required="required" class="form-control">  
                                            <center><button id="botaoRecuperar" class="btn btn-primary btn-cons pull-right" onClick="recuperar()" type="submit">Recuperar</button></center>
                                    </div>
                  </div>
                </div>
            </div>

      </div>
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="${resource(dir: 'template', file:'assets/plugins/jquery-1.8.3.min.js)}" type="text/javascript"></script>
    <script src="${resource(dir: 'template', file:'assets/plugins/bootstrap/js/bootstrap.min.js)}" type="text/javascript"></script>
    <script src="${resource(dir: 'template', file:'assets/plugins/pace/pace.min.js)}" type="text/javascript"></script>
    <script src="${resource(dir: 'template', file:'assets/plugins/jquery-validation/js/jquery.validate.min.js)}" type="text/javascript"></script>
    <script src="${resource(dir: 'template', file:'assets/js/login.js)}" type="text/javascript"></script>
    <!-- BEGIN CORE TEMPLATE JS -->
    <!-- END CORE TEMPLATE JS -->
</body>
</html>