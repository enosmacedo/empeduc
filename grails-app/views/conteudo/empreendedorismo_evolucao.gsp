<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>Webarch - Responsive Admin Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
    
<!-- BEGIN PLUGIN CSS -->
 <link href="${resource(dir: 'template', file:'plugins/pace/pace-theme-flash.css')}" rel="stylesheet" type="text/css" media="screen"/>
<!-- END PLUGIN CSS -->
<!-- BEGIN CORE CSS FRAMEWORK -->
 <link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap.min.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap-theme.min.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'plugins/font-awesome/css/font-awesome.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'css/animate.min.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.css')}" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
 <link href="${resource(dir: 'template', file:'css/style.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'css/responsive.css')}" rel="stylesheet" type="text/css"/>
 <link href="${resource(dir: 'template', file:'css/custom-icon-set.css')}" rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->
<style>
table, td{
	border: 2px solid black;
    border-collapse: collapse;
	background: blue;
	color: white;
}
td{
	padding: 5px;
    text-align: center;  
}


</style>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
	<div class="header-seperation"> 
		<ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
		 <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
		</ul>
      <!-- BEGIN LOGO -->	
      <a href="index.html"><img src="${resource(dir: 'template', file:'/img/logo.png')}" class="logo" alt=""  data-src="${resource(dir: 'template', file:'/img/logo.png')}" data-src-retina="assets/img/logo2x.png')}" width="106" height="21"/></a>
      <!-- END LOGO --> 
      <ul class="nav pull-right notifcation-center">	
        <li class="dropdown" id="header_task_bar"> <a href="index.html" class="dropdown-toggle active" data-toggle=""> <div class="iconset top-home"></div> </a> </li>
        <li class="dropdown" id="header_inbox_bar" > <a href="email.html" class="dropdown-toggle" > <div class="iconset top-messages"></div>  <span class="badge" id="msgs-badge">2</span> </a></li>
		<li class="dropdown" id="portrait-chat-toggler" style="display:none"> <a href="#sidr" class="chat-menu-toggle"> <div class="iconset top-chat-white "></div> </a> </li>        
      </ul>
      </div>
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav" > 
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu"> 
  <!-- BEGIN MINI-PROFILE -->
	   <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
		   <div class="user-info-wrapper">	
				<div class="profile-wrapper">
					<img src="${resource(dir: 'template', file:'/img/aluno.png')}"  alt="" data-src="${resource(dir: 'template', file:'/img/profiles/avatar.jpg')}" data-src-retina="assets/img/profiles/avatar2x.jpg')}" width="69" height="69" />
				</div>
				<div class="user-info">
				  <div class="greeting">Bem-vindo,</div>
				  <div class="username">Daniel <span class="semi-bold">Enos</span></div>
				  <div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div>
				</div>
		   </div>
	  <!-- END MINI-PROFILE -->
	   
	   <!-- BEGIN SIDEBAR MENU -->	
		<ul>	
		  <li class=""> <a href="/"> <i class="fa fa-flag"></i>  <span class="title">Perfil</span></a></li>

		  <li class=""> <a href="/"> <i class="fa fa-flag"></i>  <span class="title">Conteúdos</span></a></li>
				
		  <li class="active open "> <a href="javascript:;"> <i class="fa fa fa-adjust"></i> <span class="title">Fórum</span> <span class="arrow open"></span> </a>
            <ul class="sub-menu">
              <li > <a id="botao_promocao_criar" >Minhas Mensagens</a> </li>
              <li > <a id="botao_promocao_editar">Mensagens do Professor </a> </li>
			  <li > <a id="botao_promocao_premiar">Premiar </a> </li>
            </ul>
		  </li>  		
		  <li class=""> <a href="${resource(file: '../areaRestrita/login')}"> <i class="fa fa-flag"></i>  <span class="title">Sair</span></a></li>
		</ul>	
		<!-- END SIDEBAR MENU --> 
	  </div>
  </div>
  <!-- END SIDEBAR --> 
  
  
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content"> 
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">  
		<div align = "justify"  class="page-title">	
			<h3>Empreendedorismo e a evolução histórica do conceito <span class="semi-bold">Emp</span>reendedor</h3>	

			<p>Algumas vezes, você deve ter se perguntado sobre o que é empreendedorismo. Por definição, podemos dizer nesse primeiro momento que essa é uma área do conhecimento responsável por estudar o empreendedor, buscando conhecer seu universo de atuação, perfil, características e suas origens. </p> 
			<p>Em outras palavras, pode-se dizer que é também a busca por compreender melhor como surgem as oportunidades para desenvolver uma nova ideia, o processo empreendedor e planejamento de um empreendedor.</p>
			</br>
			<div align="center">
				<table>
					<tr>
						<td>O que é empreendedorismo?</td>
					</tr>
					<tr>
						<td>Eder Luiz Bolson disse : "empreendedorismo é um movimento educacional que visa desenvolver pessoas dotadas de atitudes empreendedoras e mentes planejadoras".</td>
						<td>Robert Menezes -"Empreendedorismo é a arte de fazer acontecer com motivação e criatividade.</td>
					</tr>
				</table>
			</div>	
			</br>
			<p>Vocês já se questionaram sobre quem foi o primeiro ou o maior empreendedor da história, ou ainda, quando surgiu o empreendedorismo?. Para estas perguntas não existe uma resposta unânime entre os pesquisadores e historiadores, porém o primeiro empreendedor mais aceito pelos pesquisadores foi Marco Polo, um italiano nascido em 1254 e vivenciou o início das rotas comerciais marítimas.  </p>
			<p>Segundo Dornelas (2007), Marco Polo tentou abrir rotas marítimas para o oriente, com a intenção de transportar produtos europeus e outras mercadorias para serem vendidos no oriente, mas a travessia marítima entre a Europa e o oriente era pouco conhecida e naquela época o imaginário da população era fértil e acreditavam na existência de monstros e criaturas marítimas, assim Marco Polo foi considerado “o aventureiro empreendedor”.</p>
			<p>Para aqueles que gostam de filmes e séries a dica é ver no provedor Netlflix um pouco sobre acontecimentos dessa época e da história de Marco Polo. Foram filmadas somente duas temporadas da série, mas o importante é que você ainda pode conhecer mais sobre a história desse empreendedor.</p>
			<p>No link a seguir é possível assistir o trailer da primeira temporada:  <a href="https://www.youtube.com/watch?v=3SKsZBRfm8U">https://www.youtube.com/watch?v=3SKsZBRfm8U</a> </p>
			<p>Dornelas no livro “Empreendedorismo: Transformando ideias em negócios”, apresentou um pouco da história mundial do empreendedorismo, mostrando como aconteceu até chegarmos na forma de empreendedor atual, a seguir conhecermos seus trabalhos nessa área.</p>
			<p>Na idade média o empreendedor era definido como “Pessoas que gerenciava grandes projetos de produção”. Nesta época o empreendedor era associado somente com atividades de gerenciamento, não assumia riscos e não buscavam investidores, pois os grandes projetos de produção eram financiados pelo governo.</p>
			<p>No início do século XVII, o empreendedor assumiu outra função, o empreendedor fazia um acordo contratual com o Estado para poder realizar um serviço ou fornecer produtos. No mesmo século, o economista Richard Cantillon, começou a diferenciar o empreendedor do capitalista, sendo considerado por muitos estudiosos o criador do termo empreendedorismo. De acordo com sua ideias, o empreendedor assumia riscos para poder conseguir o sucesso, diferente do capitalista, que era responsável por fornecer o capital necessário.</p>
			<p>No século seguinte, o inicio de revolução industrial foi responsável por estabelecer e firmar a diferença entre o empreendedor e o capitalista. Com isso, o economista francês Jean-Baptiste Say, definiu o empreendedor como a pessoa capaz de mover recursos ou capital de uma área de pouca produtividade para uma área com maior produtividade, gerando um retorno.</p>
			<p>No início do século XX, os empreendedores eram constantemente confundidos com administradores, e o que ocorre até os dias atuais.</p>
			<p>Agora, você pode se questionar: “Mas, existem diferenças entre empreendedores e administradores? Se existem, quais são essas diferenças?”.
			Os administradores e empreendedores são facilmente empreendedores, porque os dois são responsáveis por administrar e gerenciar negócios, mas o administrador é empregado de um capitalista, já os empreendedores assumem riscos e transforma sua ideia em negócios com o objetivo de comandar e gerenciar o seu próprio negócio.
			</p>
			<p>Importante também destacar as contribuições de Joseph Schumpeter para o entendimento do empreendedorismo. Esse economista introduziu 2 (dois) conceitos importantes para o mundo do empreendedor: inovações tecnológicas e destruição criativa.</p>
			<p>Segundo esse autor, as inovações tecnológicas funcionam com combustível para o desenvolvimento do capitalismo e a destruição criativa é a substituição de antigos produtos por novos produtos, causando a destruição de métodos antigos de produção, dinamismo das industrias e pelo crescimento econômico a longo prazo. </p>
			<p>Com o acréscimo dessas ideias, o empreendedor passou a ser uma pessoa tida como criativa e capaz de fazer sucesso com inovação, sendo considerados agentes de mudança da economia.  </p>
			<p>Anos depois, por volta da década de 70 do século XX, Peter Drucker e Kenneth E. Knight, introduziram o conceito de risco ao mundo do empreendedor. Dessa maneira, eles passaram a ser consideradas pessoas que aproveitam as oportunidades, arriscando para criar novos projetos e proporcionar mudanças. </p>
			<p>Depois de conhecer como ocorreu do surgimento do empreendedorismo até a atualidade, vocês conseguem pensar no que realmente é ser um empreendedor? </p>
			<p>Não existe somente uma resposta, o empreendedor pode ser aquela pessoa criativa, ousada, organizada e muito persistente, um visionário. Sabe-se, entretanto, que o empreendedor no contexto atual é um grande contribuidor para o desenvolvimento econômico e social, por ser um profissional inovador, que se modifica para atuar em qualquer área do conhecimento humano, sendo considerando o criador de novos negócios que geram emprego e renda.</p>

			
			Referências
			DORNELAS, Jose Carlos de Assis. Empreendedorismo: transformando ideias em negócios. 5 ed. Rio de Janeiro: Empreende / LTC, 2014.
			CHIAVENATO, Idalberto. Empreendedorismo: dando asas ao espirito empreendedor. 4 ed. Barueri, SP : Manole, 2012.
	
			
		</div>
    </div>
  </div>  
 <!-- END CONTAINER --> 
</div>
<!-- END CONTAINER -->

<!-- BEGIN CORE JS FRAMEWORK--> 
<script src="${resource(dir: 'template', file:'plugins/jquery-1.8.3.min.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/boostrapv3/js/bootstrap.min.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/breakpoints.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/jquery-unveil/jquery.unveil.min.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.min.js')}" type="text/javascript"></script>
<!-- END CORE JS FRAMEWORK --> 
<!-- BEGIN PAGE LEVEL JS --> 
<script src="${resource(dir: 'template', file:'plugins/pace/pace.min.js')}" type="text/javascript"></script>  
<script src="${resource(dir: 'template', file:'plugins/jquery-numberAnimate/jquery.animateNumbers.js')}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS --> 	

<!-- BEGIN CORE TEMPLATE JS --> 
<script src="${resource(dir: 'template', file:'js/core.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'js/chat.js')}" type="text/javascript"></script> 
<!-- END CORE TEMPLATE JS --> 
</body>
</html>