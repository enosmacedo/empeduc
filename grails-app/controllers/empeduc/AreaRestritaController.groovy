package empeduc

class AreaRestritaController {

    def index() { }
    
    def login() {
        render (view : '/areaRestrita/login')
    }
    
    def logout() {
        redirect (action : 'login')        
    }
    
    def erro() {
        render (view : '/areaRestrita/erro')        
    }
    
    def admin() { }
    
    def cadastro() { }
} 
