<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>EMPEDUC - Plataforma Web para Ensino de Empreendedorismo e Finanças</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
    
<!-- BEGIN PLUGIN CSS -->
<link href="${resource(dir: 'template', file:'plugins/pace/pace-theme-flash.css')}"  rel="stylesheet" type="text/css" media="screen"/>
<!-- END PLUGIN CSS -->
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap-theme.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/font-awesome/css/font-awesome.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/animate.min.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.css')}"  rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link href="${resource(dir: 'template', file:'css/style.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/responsive.css')}"  rel="stylesheet" type="text/css"/>
<link href="${resource(dir: 'template', file:'css/custom-icon-set.css')}"  rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
	<div class="header-seperation"> 
		<ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
		 <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
		</ul>
      <!-- BEGIN LOGO -->	
      <a href="index.html"><img src="${resource(dir: 'template', file:'/img/logo.png')}" class="logo" alt=""  data-src="${resource(dir: 'template', file:'/img/logo.png')}" data-src-retina="assets/img/logo2x.png')}" width="106" height="21"/></a>
      <!-- END LOGO --> 
      <ul class="nav pull-right notifcation-center">	
        <li class="dropdown" id="header_task_bar"> <a href="index.html" class="dropdown-toggle active" data-toggle=""> <div class="iconset top-home"></div> </a> </li>
        <li class="dropdown" id="header_inbox_bar" > <a href="email.html" class="dropdown-toggle" > <div class="iconset top-messages"></div>  <span class="badge" id="msgs-badge">2</span> </a></li>
		<li class="dropdown" id="portrait-chat-toggler" style="display:none"> <a href="#sidr" class="chat-menu-toggle"> <div class="iconset top-chat-white "></div> </a> </li>        
      </ul>
      </div>
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav" > 
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu"> 
  <!-- BEGIN MINI-PROFILE -->
	   <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
		   <div class="user-info-wrapper">	
				<div class="profile-wrapper">
					<img src="${resource(dir: 'template', file:'/img/aluno.png')}"  alt="" data-src="${resource(dir: 'template', file:'/img/profiles/avatar.jpg')}" data-src-retina="assets/img/profiles/avatar2x.jpg')}" width="69" height="69" />
				</div>
				<div class="user-info">
				  <div class="greeting">Bem-vindo,</div>
				  <div class="username">Daniel <span class="semi-bold">Enos</span></div>
				  <div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div>
				</div>
		   </div>
	  <!-- END MINI-PROFILE -->
	   
	   <!-- BEGIN SIDEBAR MENU -->	
		<ul>	
		  <li class=""> <a href="/"> <i class="fa fa-flag"></i>  <span class="title">Perfil</span></a></li>

		  <li class=""> <a href="/"> <i class="fa fa-flag"></i>  <span class="title">Conteúdos</span></a></li>
				
		  <li class="active open "> <a href="javascript:;"> <i class="fa fa fa-adjust"></i> <span class="title">Fórum</span> <span class="arrow open"></span> </a>
            <ul class="sub-menu">
              <li > <a id="botao_promocao_criar" >Minhas Mensagens</a> </li>
              <li > <a id="botao_promocao_editar">Mensagens do Professor </a> </li>
			  <li > <a id="botao_promocao_premiar">Premiar </a> </li>
            </ul>
		  </li>  		
		  <li class=""> <a href="${resource(file: '../areaRestrita/login')}"> <i class="fa fa-flag"></i>  <span class="title">Sair</span></a></li>
		</ul>	
		<!-- END SIDEBAR MENU --> 
	  </div>
  </div>
  <!-- END SIDEBAR --> 
  
  
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content"> 
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">  
		<div class="page-title">	
			<h3>Tipos de <span class="semi-bold">Emp</span>reendedores</h3>	
			
			
<div align = "justify">			

<p> São inúmeros os motivos que levam uma pessoa a ser empreendedora. A necessidade, a oportunidade, o sonho em empreender são alguns dos motivos que podem influenciar nessa escolha. Atualmente, não existe unanimidade entre os autores sobre a classificação dos tipos de empreendedores. </p> 
<p> Para Pessoa (2005), os empreendedores podem ser compreendidos em 3 (três) grandes grupos, conforme apresentado na figura abaixo:  </p> 

<div align="center">
	<img src="${resource(dir: 'template', file:'/img/tipo_emp1.png')}" width="65%" height="200px"/>
	<p>Figura 1: Tipos de Empreendedores – Semelhanças e Diferenças.</p>
	<p>Fonte: Adaptado de Pessoa (2005).</p>
</div>	

<p> Segundo a autora, o empreendedor startup são pessoas que começaram a empreender a partir de uma “grande ideia” e aproveitaram a oportunidade, ou ainda, podem ser considerados aqueles que começaram a empreender por necessidade. O empreendedor social é responsável por ter ideias que ajudam uma comunidade ou o ambiente, causando impactos sociais benéficos, na grande maioria trabalha sem nenhum fim lucrativo próprio. Por fim, o empreendedor corporativo podem ser associados ou sócios de uma empresa já existente. São os responsáveis por desenvolver novos negócios ou ideias dentro das suas empresas, com o intuído de renová-las.</p> 
<p> De forma mais completa, os tipos de empreendedores são definidos Dornelas (2007). Segundo o autor, são 8 (oito) os tipos de empreendedores, cada tipo um com características particulares e que são apresentadas a seguir.</p> 

<div align="center">
	<img src="${resource(dir: 'template', file:'/img/tipo_emp2.png')}" width="65%" height="250px"/>
	<p>Figura 2: Os oito tipos de empreendedores.</p>
	<p>Fonte: Adaptado de Dornelas (2007, p.11 a 16)</p>
</div>	

<h4><b>1. O Empreendedor Nato</b> </h4>
<p> Vocês provavelmente conhecem muitos empreendedores deste tipo, passam frequente nos programas e mídias, sãos os mais famosos, por exemplo, Silvo Santos, Bill Gates, Barão de Mauá, frequentemente mencionado nos livros de história por impulsionar o processo de industrialização brasileiro, etc.</p> 
<p> Eles possuem grandes qualidades como, por exemplo: são visionários, otimistas e se esforçam integralmente para conseguir cumprir com perfeição seus objetivos. Muitas vezes começam um negócio do nada e entram nesta área muito cedo.</p> 
<p> Como pontos de melhoria apresentam, muitas vezes, pouco conhecimento na área administrativa, o que pode prejudicá-los em caso de grandes ou rápidas mudanças no mercado.</p>

</br>

<h4><b> 2. O Empreendedor que Aprende</b> </h4>
<p> Este tipo de empreendedor pode ser considerado como “inesperado”. Geralmente é uma pessoa que encontra uma grande oportunidade de negócio e aproveita a oportunidade para criar seu novo negócio do zero, mudando totalmente sua carreira. Assim, precisam aprender a lidar com riscos, aprender a planejar, conseguir os recursos e gerenciar o próprio negócio.</p> 
<p> O seu maior desafio é aprender a lidar com um mundo totalmente novo e acostumar-se com a nova realidade, porém isto pode trazer boas consequências, como se tornar menos emocionalmente ligado ao negócio e assumir riscos calculados.</p> 

</br>

<h4><b>3. O Empreendedor Serial</b></h4>
<p>É o tipo de empreendedor apaixonado por grandes desafios. Estão continuamente em busca de novas oportunidades para criar negócios não necessariamente na mesma área de atuação. Por esse motivo, muitas vezes são pessoas que não tem afinidade com a gestão desses empreendimentos, deixando esta função para seus sócios.
<p>Sua maior qualidade é a habilidade em montar e liderar equipes, conseguir os recursos necessários para empreender, persistência, acreditar nas oportunidades e não descansar. Geralmente, nos seus currículos podem ser encontrados vários fracassos, porém eles usam esses fracassos como incentivo para continuar no desafio de empreender.

</br></br>

<h4><b>4. O Empreendedor Corporativo</b></h4>
<p>Conhecidos também como Intraempreendedores, são executivos/funcionários de empresas. São os responsáveis por criar novos produtos e serviços, mantendo a empresa sempre atualizada. Possuem grande conhecimento e experiência na área administrativa.</p>
<p>Como principais características pode-se dizer que são pessoas ambiciosas e não se contentam com o que já conquistaram. Estão sempre correndo atrás de novos desafios. A maior dificuldade que podem encontrar nas organizações é a possibilidade de não terem total liberdade para inovar e desenvolverem novos projetos, pois sempre terão alguém no comando.</p>

</br>

<h4><b>5. Empreendedor Social</b> </h4>
<p>Sua missão é construir um mudo melhor para as pessoas, se envolvendo em causas sociais ou ambientais. Este tipo de empreendedor possui objetivos diferentes dos outros tipos de empreendedores, pois não trabalham com o objetivo do lucro, mas sim de criar possibilidades para minimizar a desigualdade social. </p>
<p>O que pode impedir a continuidade dessas pessoas como empreendedores sociais é o surgimento de outras necessidades pessoais que impeçam maior dedicação a causa. Além disso, esses empreendedores podem ter suas ações confundidas com interesses obscuros/nebulosos. </p>

</br>

<h4><b>6. O Empreendedor por Necessidade</b></h4>
<p>O empreendedor por necessidade é uma pessoa que encara o negócio como última alternativa profissional para sua sobrevivência. O principal motivo de assumirem essa postura é a perda do emprego ou a necessidade de aumentar a renda da familiar. Por essas questões, não possuem preparo necessário para iniciar ou gerenciar um negócio.</p>
<p>Como não possuem conhecimento, esses negócios são normalmente pequenos e informais, disponibilizam serviços simples e mais tradicionais, proporcionam pouco retorno financeiro. Cada vez mais, tem crescido o número de empreendedores desse tipo no país devido à ausência de emprego para a população. Por outro lado, esse tipo de empreendedor apresenta como características positivas a dedicação integral ao negócio e o grande interesse que o empreendimento tenho sucesso.</p>

</br>

<h4><b>7. O Empreendedor Herdeiro</b></h4>
<p>O empreendedor herdeiro começa no mundo do negócios muito cedo, muitas vezes antes de se tornarem maiores de idade. Isso acontece como sendo uma “fase de preparação”, pois seus pais idealizam que esses, por fazerem parte da família, deverão ser os sucessores na gestão do negócio.</p>
<p>O principal desafio é multiplicar o patrimônio da família, pois quando um herdeiro entra no comando, ele será o responsável por escolher entre renovar a empresa ou conservá-la. Por isso, é importante a ajuda dos sócios ou de pessoas experientes na tomada de decisão, pois geralmente estão muito ligados emocionalmente ao negócio.</p>
<p>Como aspecto negativo, esses empreendedores podem se ver “obrigados” a trabalharem em um negócio que não gostam e apresentam resistência por inovação dentro da empresa.</p>
 
</br>
 
<h4><b>8. Empreendedor “Normal”.</b></h4>
<p>O significado de um empreendedor normal está ligado com o que se espera encontrar em empreendedor. Este tipo é o mais completo, pois planeja, assume riscos calculados, adquire conhecimento, tem uma grade rede de parceiros, boas técnicas de comunicação e gerenciamento, entre outras características.</p>
<p>O empreendedor normal também pode ser considerado como o empreendedor planejado, questão-chave para o sucesso negócio. Mesmo sendo o tipo mais completo de empreendedor, não é encontrado com frequência. São profissionais que se qualificam antes de empreender.</p>
<p>É preciso atentar que esse tipo de empreendedor também apresenta desafios à serem enfrentados como a falta de apego emocional ao negócio, o que pode fazê-lo desistir da ideia nas primeiras dificuldades enfrentadas, bem como a possibilidade de perder oportunidades pela falta de agilidade na tomada de decisão.</p>

<p>Se você gostou de conhecer os vários tipos de empreendedores, também vai gostar de ver a história dos empreendedores que criaram o McDolnald’s retratada no filme “The Founder”, ou “Fome de Poder” como é conhecido no Brasil.</p>
<p>O filme mostra a trajetória do McDonald’s desde a pequena lanchonete nos anos 50 até a gigante do fast-food dos dias atuais. Para conferir a história completa, assista ao filme e tente reconhecer os tipos de empreendedores, colocando em prática seus ensinamentos.<p>
<div align="center">
	<img src="${resource(dir: 'template', file:'/img/tipo_emp3.jpg')}" width="30%" height="350px"/>
	<p>Vale a pena conferir o trailer:</p>
	<p>https://www.youtube.com/watch?v=hpSRzLUFkN4</p>
</div>

REFERÊNCIAS
DORNELAS, José Carlos Assis. Empreendedorismo na prática: mitos e verdades do empreendedor de sucesso.1°ed. Rio de Janeiro: Elsevier, 2007.
PESSOA, Eliane. TIPOS DE EMPREENDEDORISMO: SEMELHANÇAS E DIFERENÇAS. Disponível em < http://www.administradores.com.br/artigos/negocios/tipos-de-empreendedorismo-semelhancas-e-diferencas/10993/.>. Acesso: 24 de ago. 2017

</div>			
			
			
			
		</div>
    </div>
  </div>  
 <!-- END CONTAINER --> 
</div>
<!-- END CONTAINER -->

<!-- BEGIN CORE JS FRAMEWORK--> 
<script src="${resource(dir: 'template', file:'plugins/jquery-1.8.3.min.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/boostrapv3/js/bootstrap.min.js"')}"type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/breakpoints.js" type="text/javascript')}"></script> 
<script src="${resource(dir: 'template', file:'plugins/jquery-unveil/jquery.unveil.min.js')}" type="text/javascript"></script> 
<script src="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.min.js')}" type="text/javascript"></script>
<!-- END CORE JS FRAMEWORK --> 
<!-- BEGIN PAGE LEVEL JS --> 
<script src="${resource(dir: 'template', file:'plugins/pace/pace.min.js" type="text/javascript')}"></script>  
<script src="${resource(dir: 'template', file:'plugins/jquery-numberAnimate/jquery.animateNumbers.js')}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS --> 	

<!-- BEGIN CORE TEMPLATE JS --> 
<script src="${resource(dir: 'template', file:'js/core.js" type="text/javascript')}"></script> 
<script src="${resource(dir: 'template', file:'js/chat.js" type="text/javascript')}"></script> 
<!-- END CORE TEMPLATE JS --> 
</body>
</html>