package empeduc

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class TopicoController {

    TopicoService topicoService
    ComentarioService comentarioService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond topicoService.list(params), model:[topicoCount: topicoService.count()]
    }

    def show(Long id) {
        respond topicoService.get(id)
        respond comentarioService.list(params), model:[comentarioCount: comentarioService.count()]
        
    }

    def create() {
        respond new Topico(params)
    }

    def save(Topico topico) {
        if (topico == null) {
            notFound()
            return
        }

        try {
            topicoService.save(topico)
        } catch (ValidationException e) {
            respond topico.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'topico.label', default: 'Topico'), topico.id])
                redirect topico
            }
            '*' { respond topico, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond topicoService.get(id)
    }
    
    def comentar(Long id){
        redirect (uri : '/comentario/create', params: [topico: id])        
    }

    def update(Topico topico) {
        if (topico == null) {
            notFound()
            return
        }

        try {
            topicoService.save(topico)
        } catch (ValidationException e) {
            respond topico.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'topico.label', default: 'Topico'), topico.id])
                redirect topico
            }
            '*'{ respond topico, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        topicoService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'topico.label', default: 'Topico'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'topico.label', default: 'Topico'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
