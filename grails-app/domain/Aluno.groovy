package empeduc

class Aluno extends Pessoa {

    static constraints = {
    }
    

    static hasMany = [diarios:Diario, notas:Nota]

}
