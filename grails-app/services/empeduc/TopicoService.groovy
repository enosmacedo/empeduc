package empeduc

import grails.gorm.services.Service

@Service(Topico)
interface TopicoService {

    Topico get(Serializable id)

    List<Topico> list(Map args)

    Long count()

    void delete(Serializable id)

    Topico save(Topico topico)

}