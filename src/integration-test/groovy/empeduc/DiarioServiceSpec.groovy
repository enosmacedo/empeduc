package empeduc

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class DiarioServiceSpec extends Specification {

    DiarioService diarioService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Diario(...).save(flush: true, failOnError: true)
        //new Diario(...).save(flush: true, failOnError: true)
        //Diario diario = new Diario(...).save(flush: true, failOnError: true)
        //new Diario(...).save(flush: true, failOnError: true)
        //new Diario(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //diario.id
    }

    void "test get"() {
        setupData()

        expect:
        diarioService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Diario> diarioList = diarioService.list(max: 2, offset: 2)

        then:
        diarioList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        diarioService.count() == 5
    }

    void "test delete"() {
        Long diarioId = setupData()

        expect:
        diarioService.count() == 5

        when:
        diarioService.delete(diarioId)
        sessionFactory.currentSession.flush()

        then:
        diarioService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Diario diario = new Diario()
        diarioService.save(diario)

        then:
        diario.id != null
    }
}
