function getRandomString() {
    return Math.random().toString(36).substr(2, 7);
}

function getRandomInRange(from, to, fixed) {
    return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
}

$rootScope.tablesList = {
	Message : function(obj) {
		obj = obj || new $rootScope.Classes.Message();
		obj.title = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.serialVersionUID = Number(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.text = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));

		return obj;
	},
	Reward : function(obj) {
		obj = obj || new $rootScope.Classes.Reward();
		obj.serialVersionUID = Number(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.name = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.points = Number(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));

		return obj;
	},
	Point : function(obj) {
		obj = obj || new $rootScope.Classes.Point();
		obj.points = Number(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));

		return obj;
	},
	Company : function(obj) {
		obj = obj || new $rootScope.Classes.Company();
		obj.name = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.logo = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.adress = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.email = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.phone_number = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.serialVersionUID = Number(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));

		return obj;
	},
	Promotion : function(obj) {
		obj = obj || new $rootScope.Classes.Promotion();
		obj.description = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.title = String(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));
		obj.serialVersionUID = Number(Math.abs(Math.floor(Math.random()*Math.pow(10, 5) - 1)));

		return obj;
	}
};

$rootScope.createInstanceOf = function(table) {
    return tablesList[table]();
};