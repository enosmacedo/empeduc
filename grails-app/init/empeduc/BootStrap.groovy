package empeduc

import comum.Permissao
import comum.Usuario
import comum.UsuarioPermissao

class BootStrap {

    def init = { servletContext ->
        Permissao admin = Permissao.findByAuthority("ROLE_ADMIN")
        if (admin == null) {
            admin = new Permissao (authority: "ROLE_ADMIN").save(flush: true)
        }
        Permissao professor = Permissao.findByAuthority("ROLE_PROFESSOR")
        if (professor == null) {
            professor = new Permissao (authority: "ROLE_PROFESSOR").save(flush: true)
        }
        Permissao diretor = Permissao.findByAuthority("ROLE_DIRETOR")
        if (diretor == null) {
            diretor = new Permissao (authority: "ROLE_DIRETOR").save(flush: true)
        }
        Permissao aluno = Permissao.findByAuthority("ROLE_ALUNO")
        if (aluno == null) {
            aluno = new Permissao (authority: "ROLE_ALUNO").save(flush: true)
        }
        
        
        
        Usuario admin1 = Usuario.findByUsername("admin1")
        if (admin1 == null) {
            admin1 = new Usuario(username: "admin1", password:"123", enabled:true,
                accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
        }
        Usuario aluno1 = Usuario.findByUsername("aluno1")
        if (aluno1 == null) {
            aluno1 = new Usuario(username: "aluno1", password:"123", enabled:true,
                accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
        }
        Usuario professor1 = Usuario.findByUsername("professor1")
        if (professor1 == null) {
            professor1 = new Usuario(username: "professor1", password:"123", enabled:true,
                accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
        }
        Usuario diretor1 = Usuario.findByUsername("diretor1")
        if (diretor1 == null) {
            diretor1 = new Usuario(username: "diretor1", password:"123", enabled:true,
                accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
        }
        
        
        if(UsuarioPermissao.findByUsuarioAndPermissao(admin1, admin) == null) {
            new UsuarioPermissao(usuario: admin1, permissao: admin).save(flush: true)
        }
        if(UsuarioPermissao.findByUsuarioAndPermissao(aluno1, aluno) == null) {
            new UsuarioPermissao(usuario: aluno1, permissao: aluno).save(flush: true)
        }
        if(UsuarioPermissao.findByUsuarioAndPermissao(professor1, professor) == null) {
            new UsuarioPermissao(usuario: professor1, permissao: professor).save(flush: true)
        }
        if(UsuarioPermissao.findByUsuarioAndPermissao(diretor1, diretor) == null) {
            new UsuarioPermissao(usuario: diretor1, permissao: diretor).save(flush: true)
        }
        
    }
    def destroy = {
    }
}
