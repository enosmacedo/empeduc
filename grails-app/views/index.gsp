<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>EMPEDUC</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- BEGIN PLUGIN CSS -->
        <link href="${resource(dir: 'template', file:'/plugins/pace/pace-theme-flash.css')}"  rel="stylesheet" type="text/css" media="screen"/>
        <!-- END PLUGIN CSS -->
        <!-- BEGIN CORE CSS FRAMEWORK -->
        <link href="${resource(dir: 'template', file:'/plugins/boostrapv3/css/bootstrap.min.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/plugins/boostrapv3/css/bootstrap-theme.min.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/plugins/font-awesome/css/font-awesome.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/animate.min.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/plugins/jquery-scrollbar/jquery.scrollbar.css')}"  rel="stylesheet" type="text/css"/>
        <!-- END CORE CSS FRAMEWORK -->
        <!-- BEGIN CSS TEMPLATE -->
        <link href="${resource(dir: 'template', file:'/css/style.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/responsive.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'/css/custom-icon-set.css')}"  rel="stylesheet" type="text/css"/>
        <!-- END CSS TEMPLATE -->

    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <body class="">
    <!-- BEGIN HEADER -->
        <div class="header navbar navbar-inverse "> 
          <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="navbar-inner">
                <div class="header-seperation"> 
                    <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
                        <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
                    </ul>

                </div>
                <!-- END RESPONSIVE MENU TOGGLER --> 
                <div class="header-quick-nav" > 
                    <div class="pull-right"> 
                        <div class="chat-toggler">	
                            <div class="profile-pic"> 
                                <img src="/assets/img/profiles/avatar_small.jpg"  alt="" data-src="/assets/img/profiles/avatar_small.jpg" data-src-retina="/assets/img/profiles/avatar_small2x.jpg" width="35" height="35" /> 
                            </div>
                            <div class="talkballon">
                                <div class="talk-bubble tri-right right-in">
                                    <div class="talktext">
                                        <p id="talkballon">Teste</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- END TOP NAVIGATION MENU --> 
            </div>
            <!-- END TOP NAVIGATION BAR --> 
        </div>
        <!-- END HEADER -->


        <g:render template="menulateral" />


            <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content"> 
          <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="content">  
                <div class="page-title">	
                    <h3>Conteúdos</h3>

                        <!-- BEGIN --> 
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h4>Emp<span class="semi-bold">reendedorismo</span></h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#grid-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="grid-body no-border">
                            <div class="row-fluid">
                                    <!-- Conteudo -->
                                <a href="conteudo/processo_empreendedor"> Processo Empreendedor </a> <hr/>
                                <a href="conteudo/empreendedorismo_brasil">O surgimento do empreendedorismo no Brasil</a> <hr/>
                                <a href="conteudo/empreendedorismo_evolucao">Empreendedorismo e a evolução histórica do conceito</a> <hr/>
                                <a href="conteudo/tipos_empreendedores"> Tipos de Empreendedores - Empreendedor Nato / Mitológico </a> <hr/>				
                                <b> Contextualização  </b></br>
                                <a href="" style="margin-left:10px"> Mundo</a></br>
                                <a href="" style="margin-left:10px"> Brasil</a></br>
                                <hr/>
                                <b> Plano de Negócios  </b></br>
                                <a href="" style="margin-left:10px"> Estrutura do Plano </a></br>
                                <a href="" style="margin-left:10px"> Aspectos Legais e Apoio para o negócio </a></br>
                                <hr/>
                                <b> Características do Comportamento Empreendedor  </b></br>
                                <a href="" style="margin-left:10px"> Realização </a></br>
                                <a href="" style="margin-left:10px"> Planejamento </a></br>
                                <a href="" style="margin-left:10px"> Poder </a></br>
                                <hr/>
                                <a href="conteudo/mitos_e_verdade_empreendimento"> Teste seus conhecimentos </a> <hr/>
                                <a href="conteudo/GameEdu"> GAME EDU </a> <hr/>
                            </div>
                        </div>
                    </div>

                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h4>Mercado <span class="semi-bold">Financeiro</span></h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#grid-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="grid-body no-border">
                            <div class="row-fluid">
                                    <!-- BEGIN BODY --> 
                                <a href=""> Bolsa de Valores </a> <hr/>
                                <a href=""> Opções </a> <hr/>
                                <a href=""> Dollar </a> <hr/>
                                <a href=""> Ações </a> <hr/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
       <!-- END CONTAINER --> 
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN CORE JS FRAMEWORK--> 
    <script src="${resource(dir: 'template', file:'/plugins/jquery-1.8.3.min.js')}" type="text/javascript"></script> 
    <script src="${resource(dir: 'template', file:'/plugins/boostrapv3/js/bootstrap.min.js"')}"type="text/javascript"></script> 
    <script src="${resource(dir: 'template', file:'/plugins/breakpoints.js" type="text/javascript')}"></script> 
    <script src="${resource(dir: 'template', file:'/plugins/jquery-unveil/jquery.unveil.min.js')}" type="text/javascript"></script> 
    <script src="${resource(dir: 'template', file:'/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK --> 
    <!-- BEGIN PAGE LEVEL JS --> 
    <script src="${resource(dir: 'template', file:'/plugins/pace/pace.min.js" type="text/javascript')}"></script>  
    <script src="${resource(dir: 'template', file:'/plugins/jquery-numberAnimate/jquery.animateNumbers.js')}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS --> 	

<!-- BEGIN CORE TEMPLATE JS --> 
    <script src="${resource(dir: 'template', file:'/js/core.js" type="text/javascript')}"></script> 
    <script src="${resource(dir: 'template', file:'/js/chat.js" type="text/javascript')}"></script> 
    <!-- END CORE TEMPLATE JS --> 
    <script>
        $(document).ready(function(){
            function setFala(texto,elemento){
              $(elemento).text(texto);
            }

            function setTempo(tempo,elemento,elemento2){
                $(elemento).click(function(){
                $(elemento2).show()
                .slideUp(tempo);
                });
             }

                $(".talkballon").hide();

                setFala("Sou seu professor e estou aqui pra te ajudar sempre que precisar!","#talkballon");
                setTempo(5250,".profile-pic",".talkballon");

            });
    </script>

</body>
</html>