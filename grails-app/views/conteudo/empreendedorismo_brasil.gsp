<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>Webarch - Responsive Admin Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />

<!-- BEGIN PLUGIN CSS -->
        <link href="${resource(dir: 'template', file:'plugins/pace/pace-theme-flash.css')}"  rel="stylesheet" type="text/css" media="screen"/>
        <!-- END PLUGIN CSS -->
        <!-- BEGIN CORE CSS FRAMEWORK -->
        <link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap.min.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'plugins/boostrapv3/css/bootstrap-theme.min.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'plugins/font-awesome/css/font-awesome.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'css/animate.min.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.css')}"  rel="stylesheet" type="text/css"/>
        <!-- END CORE CSS FRAMEWORK -->
        <!-- BEGIN CSS TEMPLATE -->
        <link href="${resource(dir: 'template', file:'css/style.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'css/responsive.css')}"  rel="stylesheet" type="text/css"/>
        <link href="${resource(dir: 'template', file:'css/custom-icon-set.css')}"  rel="stylesheet" type="text/css"/>
        <!-- END CSS TEMPLATE -->

    </head>
    <!-- END HEAD -->

<!-- BEGIN BODY -->
    <body class="">
    <!-- BEGIN HEADER -->
        <div class="header navbar navbar-inverse "> 
          <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="navbar-inner">
                <div class="header-seperation"> 
                    <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
                        <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
                    </ul>
          <!-- BEGIN LOGO -->	
                    <a href="index.html"><img src="${resource(dir: 'template', file:'/img/logo.png')}" class="logo" alt=""  data-src="${resource(dir: 'template', file:'/img/logo.png')}" data-src-retina="assets/img/logo2x.png')}" width="106" height="21"/></a>
                    <!-- END LOGO --> 
                    <ul class="nav pull-right notifcation-center">	
                        <li class="dropdown" id="header_task_bar"> <a href="index.html" class="dropdown-toggle active" data-toggle=""> <div class="iconset top-home"></div> </a> </li>
                        <li class="dropdown" id="header_inbox_bar" > <a href="email.html" class="dropdown-toggle" > <div class="iconset top-messages"></div>  <span class="badge" id="msgs-badge">2</span> </a></li>
                        <li class="dropdown" id="portrait-chat-toggler" style="display:none"> <a href="#sidr" class="chat-menu-toggle"> <div class="iconset top-chat-white "></div> </a> </li>        
                    </ul>
                </div>
                <!-- END RESPONSIVE MENU TOGGLER --> 
                <div class="header-quick-nav" > 
                </div> 
                <!-- END TOP NAVIGATION MENU --> 
            </div>
            <!-- END TOP NAVIGATION BAR --> 
        </div>
        <!-- END HEADER -->


        <g:render template="../menulateral" />


<!-- BEGIN PAGE CONTAINER-->
        <div class="page-content"> 
          <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="content">  
                <div align = "justify"  class="page-title">	
                    <center><h3>O surgimento do <span class="semi-bold">Emp</span>reendedorismo no Brasil</h3></center>

                    <p>Você pode estar se perguntando: “como surgiu esse movimento de empreendedorismo no nosso país?”. Para responder esse pergunta, é preciso resgatar alguns acontecimentos históricos importantes de nossa história.</p>
                    <p>O empreendedorismo no Brasil começou com a abertura da nação para a industrialização, no século XIX, enquanto o Brasil se abre para a industrialização, os países europeus estão passando pela segunda revolução industrial, assim o Brasil é um pais de industrialização tardia, o que comprometeu o desenvolvimento econômico. Com a vinda de indústrias para o brasil, chegou também empresários e grandes capitalistas, o que proporcionou um grande desenvolvimento na infraestrutura nacional, o aparecimento de indústrias e empresas com capital estrangeiro e capital acumulado na exportação do café brasileiro.</p>
                    <p>Considerado o primeiro grande empreendedor brasileiro, Irineu Evangelista de Sousa, conhecido como Barão de Mauá, foi responsável pela criação do primeiro Banco do Brasil, desenvolveu o sistema ferroviário, implantação da primeira fundição de ferro, entre outras conquistas. </p>
                    <p>Em 1999, foi lançado o filme nacional “Mauá – O Imperador e o Rei”. O filme apresenta a história do Barão de Mauá, da ascensão à sua falência, demonstrando porque esse é considerado o primeiro grande empreendedor brasileiro que se tem notícia. Para maiores detalhes sobre esse obra cinematográfica, veja o link do trailer no seguinte endereço eletrônico: <a href="https://www.youtube.com/watch?v=rNVGD-kMcRs">https://www.youtube.com/watch?v=rNVGD-kMcRs.</a></p>
                    <p>Dando um salto na história, chegamos a década de 90 do século XX, onde é possível verificar o chamado surto do empreendedorismo no Brasil, com a criação de instituições como o Sebrae e Sociedade Brasileira para Exportação de Software. Antes disso, o empreendedorismo era pouco comentado no Brasil e praticamente não havia incentivo do governo nacional para desenvolver novas empresas.</p>
                    <p>Em 1990, o Brasil entrou em um processo de abertura da economia, com a abertura para produtos importados e mais baratos que os produtos nacionais, alguns setores da economia nacional passaram por problemas, pois não conseguia competir com os produtos estrangeiros. Como solução, as empresas investiram em modernização da linha de produção e administração, para ganhar espaço no mercado e voltar a competir e crescer.</p>
                    <p>O governo federal, na mesma época, começou uma série de reformas econômicas, controlou a inflação e estabilizou a economia nacional, assim o Brasil ganhou o respeito internacional e investimentos estrangeiros, proporcionado um crescimento econômico.</p>

                    <center><h3>Empreendedorismo no Brasil na Atualidade</h3></center>

                    <p>Você já deve ter escutado nas mídias que os brasileiros são favoráveis a atividades empreendedoras. Segundo o Global Entrepreneurship Monitor – GEM (2015), entre 70% e 80% dos brasileiros pensam que começar o próprio negócio e gerir sua própria empresa é uma boa carreira e valorizam as histórias que a mídia divulga de grandes empreendedores nacionais que começaram do nada e atingiram o sucesso.</p>
                    <p>O GEM (2015) realizou uma pesquisa com os empreendedores brasileiros e chegou aos seguintes resultados:</p>
                    <ul>
                        <li>7% dos empreendedores iniciantes e 5 % dos empreendedores já estáveis no mercado possuem ensino superior completo.</li>
                        <li>44% dos empreendedores iniciantes e 56% dos já estáveis no mercado possuem um grau de escolaridade abaixo do segundo grau. </li>
                    </ul>
                    <p>Comparando os dados brasileiros com os países desenvolvidos, conclui que nos países mais desenvolvidos os empreendedores têm maior grau de escolaridade, 58% dos empreendedores nos países desenvolvidos possuem curso superior completo.</p>
                    <p>Com a análise dos resultados, pode-se concluir que quanto mais alto for o nível de escolaridade de um pais, maior será a proporção de empreendedorismo por oportunidade.</p>
                    <p>E como se configura o empreendedorismo no Brasil nos últimos anos? O GEM divulgou uma pesquisa, realizada entre 2014 e 2016, que mostra como se apresenta o empreendedorismo nacional.</p>
                    <p>A pesquisa divide os empreendedores em 2 grupos:</p>
                    <ul>
                        <li>Empreendedores iniciais, com menos de 3,5 anos de atividade no ramo de empreendedorismo.</li>
                        <li>Empreendedores estabelecidos, com mais de 3,5 anos de negócios. </li>
                    </ul>
                    <p>Nos países desenvolvidos e subdesenvolvidos a taxa de empreendedores iniciais (TEA) é geralmente liderado pelo gênero masculino, porém no Brasil está taxa de empreendedores do gênero masculino é quase igual ao do gênero feminino.</p>
                    <p>Quando se trata da taxa de empreendedores estabelecidos (TEE), no Brasil, é liderada pelo gênero masculino, ou seja, homens e mulheres conseguem iniciar um negócio, mas no decorrer do processo empreendedor, muitas mulheres desistem de empreender.</p>
                    <p>No Brasil, homens e mulheres tem rotinas diferentes, as mulheres têm uma carga horaria semanal de trabalho doméstico muito maior que os homens, o que influência o modo de comandar os negócios. </p>
                    <p>De 2004 a 2014 houve um crescimento superior a 60% das mulheres responsáveis por comandar as tarefas de casa e usar a atividade empreendedora com fonte de renda. Desse modo, para ocorrer uma maior participação feminina na TEE, é preciso que haja incentivos que apoiem as mulheres a não desistirem do negócio. Ações como, por exemplo, programas de educação empreendedora que ajude a coordenar a atividade empreendedora com as atividades cotidianas direcionado tanto para os homens e mulheres.    </p>
                    <p>Em relação a faixa etária, os empreendedores nacionais coincidiram com os outros países participantes da pesquisa. A TEA, o maior grupo de empreendedores iniciantes apresenta de 25 a 34 anos e o grupo com menor participante apresenta entre 55 e 64 anos. A TEE mostra que o maior grupo de empreendedores já estabelecidos apresenta 45 a 54 anos e o menor grupo apresenta entre 55 e 64 anos. </p>
                    <p>Quando se analisa a escolaridade dos empreendedores iniciantes nacionais, o Brasil é o único país pesquisado que o maior grupo de empreendedores apresentam o primeiro grau completo e segundo grau incompleto, ou seja, muitos brasileiros começam a empreender com poucos conhecimentos na área, o que os prejudicam durante o processo empreendedor. </p>
                    <p>Analisando A TEE, o maior grupo de empreendedores experientes apresenta o segundo grau incompleto. Portanto, o empreendedorismo por necessidade é o mais decorrente no Brasil, assim é necessário alguns programas e incentivos que ajudem os empreendedores por necessidade a obter maior conhecimento na área, facilitando o sucesso dos seus negócios e proporcionado uma melhoria na administração.</p>

                    <h3>Referências</h3>

                    <p>DORNELAS, Jose Carlos de Assis. Empreendedorismo: transformando ideias em negócios. 5 ed. Rio de Janeiro: Empreende / LTC, 2014.</p>
                    <p>CALDAS, Nidia. EMPREENDEDOR: Uma análise sobre a taxa de empreendedorismo no Brasil. Disponível em:</p><a href="https://www.sebrae.com.br/sites/PortalSebrae/artigos/uma-analise-sobre-a-taxa-de-empreendedorismo-no-brasil,6a2c3e831153e510VgnVCM1000004c00210aRCRD"> https://www.sebrae.com.br/sites/PortalSebrae/artigos/uma-analise-sobre-a-taxa-de-empreendedorismo-no-brasil,6a2c3e831153e510VgnVCM1000004c00210aRCRD</a>. Acesso 01/nov. /2017.



                </div>
            </div>
        </div>  
       <!-- END CONTAINER --> 
    </div>
    <!-- END CONTAINER -->

<!-- BEGIN CORE JS FRAMEWORK--> 
    <script src="${resource(dir: 'template', file:'plugins/jquery-1.8.3.min.js')}" type="text/javascript"></script> 
    <script src="${resource(dir: 'template', file:'plugins/boostrapv3/js/bootstrap.min.js"')}"type="text/javascript"></script> 
    <script src="${resource(dir: 'template', file:'plugins/breakpoints.js" type="text/javascript')}"></script> 
    <script src="${resource(dir: 'template', file:'plugins/jquery-unveil/jquery.unveil.min.js')}" type="text/javascript"></script> 
    <script src="${resource(dir: 'template', file:'plugins/jquery-scrollbar/jquery.scrollbar.min.js')}" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK --> 
    <!-- BEGIN PAGE LEVEL JS --> 
    <script src="${resource(dir: 'template', file:'plugins/pace/pace.min.js" type="text/javascript')}"></script>  
    <script src="${resource(dir: 'template', file:'plugins/jquery-numberAnimate/jquery.animateNumbers.js')}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS --> 	

<!-- BEGIN CORE TEMPLATE JS --> 
    <script src="${resource(dir: 'template', file:'js/core.js" type="text/javascript')}"></script> 
    <script src="${resource(dir: 'template', file:'js/chat.js" type="text/javascript')}"></script> 
    <!-- END CORE TEMPLATE JS --> 
</body>
</html>
