package empeduc

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class TopicoServiceSpec extends Specification {

    TopicoService topicoService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Topico(...).save(flush: true, failOnError: true)
        //new Topico(...).save(flush: true, failOnError: true)
        //Topico topico = new Topico(...).save(flush: true, failOnError: true)
        //new Topico(...).save(flush: true, failOnError: true)
        //new Topico(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //topico.id
    }

    void "test get"() {
        setupData()

        expect:
        topicoService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Topico> topicoList = topicoService.list(max: 2, offset: 2)

        then:
        topicoList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        topicoService.count() == 5
    }

    void "test delete"() {
        Long topicoId = setupData()

        expect:
        topicoService.count() == 5

        when:
        topicoService.delete(topicoId)
        sessionFactory.currentSession.flush()

        then:
        topicoService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Topico topico = new Topico()
        topicoService.save(topico)

        then:
        topico.id != null
    }
}
