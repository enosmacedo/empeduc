package empeduc

import grails.gorm.services.Service

@Service(Diario)
interface DiarioService {

    Diario get(Serializable id)

    List<Diario> list(Map args)

    Long count()

    void delete(Serializable id)

    Diario save(Diario diario)

}