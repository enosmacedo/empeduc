package empeduc

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

import comum.Permissao
import comum.Usuario
import comum.UsuarioPermissao

class DiretorController {
    
    def springSecurityService

    DiretorService diretorService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond diretorService.list(params), model:[diretorCount: diretorService.count()]
    }

    def show(Long id) {
        
        Permissao diretor = Permissao.findByAuthority("ROLE_DIRETOR")
        if (diretor == null) {
            diretor = new Permissao (authority: "ROLE_DIRETOR").save(flush: true)
        }
        
        Usuario diretor1 = Usuario.findByUsername(diretorService.get(id).email)
        if (diretor1 == null) {
            diretor1 = new Usuario(username: diretorService.get(id).email, password: diretorService.get(id).senha, enabled:true,
                accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
        }
        
        if(UsuarioPermissao.findByUsuarioAndPermissao(diretor1, diretor) == null) {
            new UsuarioPermissao(usuario: diretor1, permissao: diretor).save(flush: true)
        }
        
        respond diretorService.get(id)
    }
    
    def perfil(Long id){
        
        String a = diretorService.list(params)
        String aux = a.charAt(a.length()-2)
        
        int i = 2
        
        while(true){
           
            if(diretorService.get(aux).email == springSecurityService.principal.username){
                int teste = Integer.parseInt(aux)
                id = teste
                break
            }
            else{
                i+=21
                aux = a.charAt(a.length()-i)
                
            }
             
        }
        
        redirect (uri : '/diretor/show/' + id)        
    }

    def create() {
        respond new Diretor(params)
    }

    def save(Diretor diretor) {
        if (diretor == null) {
            notFound()
            return
        }

        try {
            diretorService.save(diretor)
        } catch (ValidationException e) {
            respond diretor.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'diretor.label', default: 'Diretor'), diretor.id])
                redirect diretor
            }
            '*' { respond diretor, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond diretorService.get(id)
    }

    def update(Diretor diretor) {
        if (diretor == null) {
            notFound()
            return
        }

        try {
            diretorService.save(diretor)
        } catch (ValidationException e) {
            respond diretor.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'diretor.label', default: 'Diretor'), diretor.id])
                redirect diretor
            }
            '*'{ respond diretor, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        diretorService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'diretor.label', default: 'Diretor'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'diretor.label', default: 'Diretor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
