package empeduc

import grails.gorm.services.Service

@Service(Diretor)
interface DiretorService {

    Diretor get(Serializable id)

    List<Diretor> list(Map args)

    Long count()

    void delete(Serializable id)

    Diretor save(Diretor diretor)

}