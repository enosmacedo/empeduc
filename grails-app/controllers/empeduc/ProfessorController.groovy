package empeduc

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

import comum.Permissao
import comum.Usuario
import comum.UsuarioPermissao

class ProfessorController {
    
    def springSecurityService

    ProfessorService professorService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond professorService.list(params), model:[professorCount: professorService.count()]
    }
    
    def perfil(Long id){
        
        String a = professorService.list(params)
        String aux = a.charAt(a.length()-2)
        
        int i = 2
        
        
        while(true){
            
            if(professorService.get(aux).email == springSecurityService.principal.username){
                int teste = Integer.parseInt(aux)
                id = teste
                break
            }
            else{
                i+=23
                aux = a.charAt(a.length()-i)
                
            }
             
        }
        
        redirect (uri : '/professor/show/' + id)        
    }

    def show(Long id) {
        
        Permissao professor = Permissao.findByAuthority("ROLE_PROFESSOR")
        if (professor == null) {
            professor = new Permissao (authority: "ROLE_PROFESSOR").save(flush: true)
        }
        
        Usuario professor1 = Usuario.findByUsername(professorService.get(id).email)
        if (professor1 == null) {
            professor1 = new Usuario(username: professorService.get(id).email, password: professorService.get(id).senha, enabled:true,
                accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
        }
        
        if(UsuarioPermissao.findByUsuarioAndPermissao(professor1, professor) == null) {
            new UsuarioPermissao(usuario: professor1, permissao: professor).save(flush: true)
        }
        
        respond professorService.get(id)
    }

    def create() {
        respond new Professor(params)
    }

    def save(Professor professor) {
        if (professor == null) {
            notFound()
            return
        }

        try {
            professorService.save(professor)
        } catch (ValidationException e) {
            respond professor.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'professor.label', default: 'Professor'), professor.id])
                redirect professor
            }
            '*' { respond professor, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond professorService.get(id)
    }

    def update(Professor professor) {
        if (professor == null) {
            notFound()
            return
        }

        try {
            professorService.save(professor)
        } catch (ValidationException e) {
            respond professor.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'professor.label', default: 'Professor'), professor.id])
                redirect professor
            }
            '*'{ respond professor, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        professorService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'professor.label', default: 'Professor'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'professor.label', default: 'Professor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
