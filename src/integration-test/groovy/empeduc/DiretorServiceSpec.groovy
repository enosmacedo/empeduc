package empeduc

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class DiretorServiceSpec extends Specification {

    DiretorService diretorService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Diretor(...).save(flush: true, failOnError: true)
        //new Diretor(...).save(flush: true, failOnError: true)
        //Diretor diretor = new Diretor(...).save(flush: true, failOnError: true)
        //new Diretor(...).save(flush: true, failOnError: true)
        //new Diretor(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //diretor.id
    }

    void "test get"() {
        setupData()

        expect:
        diretorService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Diretor> diretorList = diretorService.list(max: 2, offset: 2)

        then:
        diretorList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        diretorService.count() == 5
    }

    void "test delete"() {
        Long diretorId = setupData()

        expect:
        diretorService.count() == 5

        when:
        diretorService.delete(diretorId)
        sessionFactory.currentSession.flush()

        then:
        diretorService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Diretor diretor = new Diretor()
        diretorService.save(diretor)

        then:
        diretor.id != null
    }
}
